from .models import CustomUser, UserFollowers, Office


def by_who_context(request):
    sponsor = CustomUser.objects.filter(is_staff=False)
    return {'by_who_context': sponsor}


def users_context(request):
    all_users = CustomUser.objects.filter(is_staff=False).count()
    return {'all_users': all_users}


def offices_context(request):
    offices = Office.objects.all()
    return {'all_offices': offices}
