from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from users.models import CustomUser, Office, UserFollowers, OperationHistory


class CustomUserAdmin(ImportExportModelAdmin):
    list_display = ('username', 'user_balance', 'status', 'user_by_whom', 'user_image')
    list_filter = ('user_registration_date', 'updated', 'status')
    search_fields = ('username',)


class UserFollowersAdmin(admin.ModelAdmin):
    list_display = ('top_user', 'follower_user', 'follower_date')
    list_display_links = ('top_user', 'follower_user', 'follower_date')


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Office)
admin.site.register(UserFollowers, UserFollowersAdmin)
admin.site.register(OperationHistory)
