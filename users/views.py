from datetime import date
from django.contrib.auth.mixins import UserPassesTestMixin
from django.shortcuts import redirect, render
from django.urls import reverse_lazy, reverse
from django.views.generic import *
from users.forms import *
from users.models import CustomUser, UserFollowers, Office, OperationHistory
from django.db.models import Q
from balance.models import History, History13
from django.http import HttpResponse
from django.contrib.auth.views import LoginView
from xlwt import Borders, Alignment, Style, Pattern
from _datetime import datetime
import xlwt


def users_and_history(context):
    reg_date_query = CustomUser.objects.filter(
        Q(user_registration_date__contains=date.today()))
    money_given = History.objects.filter(history_date__contains=date.today())
    money_for_13 = History13.objects.filter(history_date__contains=date.today())
    total = 0
    second_tour = CustomUser.objects.filter(user_second_tour=True).count()
    for x in money_given:
        total += x.history_amount
    for x in money_for_13:
        total += x.history_amount13
    context['reg_date'] = reg_date_query
    context['history_total'] = total
    context['second_tour'] = second_tour
    return


class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('confirm')
    template_name = 'registration/signup.html'


class SignUpConfirmPage(TemplateView):
    template_name = 'registration/confirm.html'


class PassportUploadView(UserPassesTestMixin, UpdateView):
    model = CustomUser
    template_name = 'users/passport_upload.html'
    fields = ['user_passport', 'user_image', 'user_fullname', 'user_phone']

    def get_success_url(self):
        return reverse('user_detail', kwargs={'pk': self.object.pk})

    def test_func(self):
        return self.request.user.is_staff or CustomUser.objects.filter(username=self.request.user)


class UserList(UserPassesTestMixin, ListView):
    template_name = 'users/users_list.html'
    login_url = 'registration/login.html'
    paginate_by = 30

    def get_queryset(self):
        object_list = CustomUser.objects.filter(Q(is_staff=False) & Q(is_cashier=False)).order_by('-updated')

        query = self.request.GET.get('q')
        if query is not None:
            object_list = object_list.filter(
                Q(username__icontains=query) | Q(user_fullname__icontains=query) |
                Q(user_by_whom__username__icontains=query) |
                Q(user_notes__icontains=query) | Q(user_phone__icontains=query)
            )

        region = self.request.GET.get('user_office')
        if region != 'null' and region is not None:
            print('region is', region)
            object_list = object_list.filter(user_office__id=region)

        return object_list

    def test_func(self):
        return self.request.user.is_staff or self.request.user.is_cashier

    def get_context_data(self, **kwargs):
        context = super(UserList, self).get_context_data(**kwargs)
        users_and_history(context)
        context['search_input'] = self.request.GET.get('q')
        region = self.request.GET.get('user_office')
        if region != 'null' and region is not None:
            office = Office.objects.get(id=self.request.GET.get('user_office'))
            context['user_office_context'] = office
        return context


class UserWaitList(UserPassesTestMixin, ListView):
    template_name = 'users/users_list.html'
    login_url = 'registration/login.html'
    paginate_by = 30

    def get_queryset(self):
        return CustomUser.objects.filter(Q(status=False, is_staff=False, is_cashier=False)).order_by('-updated')

    def test_func(self):
        return self.request.user.is_staff or self.request.user.is_cashier

    def get_context_data(self, **kwargs):
        context = super(UserWaitList, self).get_context_data(**kwargs)
        users_and_history(context)
        return context


class UsersSecondTour(UserPassesTestMixin, ListView):
    template_name = 'users/users_second_tour.html'
    login_url = 'registration/login.html'
    paginate_by = 30

    def get_queryset(self):
        return CustomUser.objects.filter(
            Q(is_staff=False, is_cashier=False, user_second_tour=True, user_second_status=False)).order_by(
            '-updated')

    def test_func(self):
        return self.request.user.is_staff or self.request.user.is_cashier

    def get_context_data(self, **kwargs):
        context = super(UsersSecondTour, self).get_context_data(**kwargs)
        users_and_history(context)
        return context


class UserRegTodayList(UserPassesTestMixin, ListView):
    template_name = 'users/users_list.html'
    login_url = 'registration/login.html'
    paginate_by = 30

    def get_queryset(self):
        new_users = CustomUser.objects.filter(
            Q(user_registration_date__contains=date.today()))
        return new_users

    def test_func(self):
        return self.request.user.is_staff or self.request.user.is_cashier

    def get_context_data(self, **kwargs):
        context = super(UserRegTodayList, self).get_context_data(**kwargs)
        users_and_history(context)
        return context


class UserList75(UserPassesTestMixin, ListView):
    template_name = 'users/users_list.html'
    login_url = 'registration/login.html'
    paginate_by = 30

    def get_queryset(self):
        return CustomUser.objects.filter(is_staff=False, status=True).order_by('-updated')

    def test_func(self):
        return self.request.user.is_staff or self.request.user.is_cashier


class SearchResultsView(UserPassesTestMixin, ListView):
    model = CustomUser
    template_name = 'search_results.html'

    def test_func(self):
        return self.request.user.is_staff or self.request.user.is_cashier

    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = CustomUser.objects.filter(
            Q(username__icontains=query) | Q(user_fullname__icontains=query) | Q(
                user_by_whom__username__icontains=query) |
            Q(user_notes__icontains=query) | Q(user_phone__icontains=query) | Q(user_notes_date__icontains=query) |
            Q(user_notes__icontains=query)
        )
        return object_list

    def get_context_data(self, **kwargs):
        context = super(SearchResultsView, self).get_context_data(**kwargs)
        context['search_input'] = self.request.GET.get('q')

        return context


class UserDetail(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'users/user_details.html'
    context_object_name = 'detail'

    def test_func(self, **kwargs):
        object_user = self.kwargs['pk']
        test_user = self.request.user
        staff = test_user.is_staff
        cashier = test_user.is_cashier
        return staff or cashier or test_user.id == object_user or \
               test_user.follower1.id == object_user or test_user.follower2.id == object_user

    def get_context_data(self, **kwargs):
        context = super(UserDetail, self).get_context_data(**kwargs)
        top = CustomUser.objects.get(pk=self.kwargs['pk'])
        context['money_transactions'] = OperationHistory.objects.filter(
            Q(oper_his_user=top) & Q(oper_his_action='money')).order_by('-id')[:50]
        context['user_followers_context'] = UserFollowers.objects.filter(top_user=top).order_by('-id')[:20]

        def user_frozen(u_t_b, u_b, u_s_s, u_id, u_f=0):
            if u_t_b >= 15000 and not u_s_s:
                if u_t_b - u_b >= 15000:
                    if u_b >= 13000:
                        user_info = CustomUser.objects.get(pk=u_id)
                        user_info.user_second_tour = True
                        user_info.save()
                    u_f = u_b
                    u_b = 0
                else:
                    check = 15000 - (u_t_b - u_b)
                    u_f = u_b - check
                    if u_f >= 13000:
                        user_info = CustomUser.objects.get(pk=u_id)
                        user_info.user_second_tour = True
                        user_info.save()
                    u_b = check

            return {'user_balance': u_b, 'user_frozen': u_f, 'user_total': u_t_b}

        money_card_top = user_frozen(top.user_total_balance, top.user_balance, top.user_second_status, top.id)
        if top.follower1:
            fir = top.follower1
            money_card_first = user_frozen(fir.user_total_balance, fir.user_balance,
                                           fir.user_second_status, fir.id)
        else:
            money_card_first = 0
        if top.follower2:
            sec = top.follower2
            money_card_second = user_frozen(sec.user_total_balance, sec.user_balance,
                                            sec.user_second_status, sec.id)
        else:
            money_card_second = 0

        new_balance = {'top': money_card_top, 'first': money_card_first, 'second': money_card_second}
        context['new_balance'] = new_balance
        return context


class UserBalance(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'users/user_balance.html'

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_cashier or CustomUser.objects.filter(
            username=self.request.user)


class UserUpdate(UserPassesTestMixin, UpdateView):
    model = CustomUser
    form_class = CustomUserChangeForm
    template_name = 'users/user_update.html'

    def form_valid(self, form):
        oper = CustomUser.objects.get(pk=self.kwargs['oper'])
        form.instance.updating_operator = oper
        return super(UserUpdate, self).form_valid(form)

    def test_func(self):
        return self.request.user.is_superuser

    def get_success_url(self):
        return self.request.GET.get('next', reverse('users_list'))


class BelgiUpdates(UserPassesTestMixin, UpdateView):
    model = CustomUser
    template_name = 'users/belgi_update.html'
    fields = ['product', 'user_notes']

    def form_valid(self, form):
        oper = CustomUser.objects.get(pk=self.kwargs['oper'])
        form.instance.updating_operator = oper
        return super(BelgiUpdates, self).form_valid(form)

    def test_func(self):
        return self.request.user.is_staff or self.request.user.is_cashier

    def get_success_url(self):
        return self.request.GET.get('next', reverse('users_list'))


class UserSecondUpdate(UserPassesTestMixin, UpdateView):
    model = CustomUser
    template_name = 'users/user_second_update.html'
    fields = ['user_second_status']

    def form_valid(self, form):
        oper = CustomUser.objects.get(pk=self.kwargs['oper'])
        form.instance.updating_operator = oper
        if form.instance.user_second_status:
            form.instance.user_second_tour = False
            form.instance.user_balance -= 13000
            History.objects.create(history_username=form.instance.username, history_user_id=form.instance.id,
                                   history_user_fullname=form.instance.user_fullname, history_amount=13000,
                                   history_remark='Переведен на 2 этап')
        return super(UserSecondUpdate, self).form_valid(form)

    def test_func(self):
        return self.request.user.is_staff or self.request.user.is_cashier

    def get_success_url(self):
        return self.request.GET.get('next', reverse('users_second_tour'))


class UserDelete(UserPassesTestMixin, DeleteView):
    model = CustomUser
    template_name = 'users/user_delete.html'

    def test_func(self):
        return self.request.user.is_superuser

    def get_success_url(self):
        return self.request.GET.get('next', reverse('users_list'))


class UserMatrixView(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'users/new_matrix.html'

    def test_func(self):
        return self.request.user.is_authenticated

    def get_context_data(self, **kwargs):
        context = super(UserMatrixView, self).get_context_data(**kwargs)
        list_matrix = []

        def check_matrix(pk_number, count):
            print(count)
            top_matrix = CustomUser.objects.get(pk=pk_number)
            by = ''
            if top_matrix.user_by_whom:
                by = top_matrix.user_by_whom.id
            else:
                by = 'null'
            list_matrix.append(
                {"key": top_matrix.id, "name": top_matrix.username, 'title': top_matrix.user_fullname, "parent": by,
                 })
            if count <= 10:
                if top_matrix.follower1:
                    count += 1
                    check_matrix(top_matrix.follower1.pk, count)
                count -= 1
                if top_matrix.follower2:
                    count += 1
                    check_matrix(top_matrix.follower2.pk, count)
            return

        counter = 0
        check_matrix(self.kwargs['pk'], counter)
        context['data'] = list_matrix
        return context


class UserListMatrix(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'users/matrix.html'

    def test_func(self):
        return self.request.user.is_authenticated

    def get_context_data(self, **kwargs):
        context = super(UserListMatrix, self).get_context_data(**kwargs)
        list_matrix = []

        def check_matrix(pk_number, count, fol=0):
            top_matrix = CustomUser.objects.get(pk=pk_number)
            by = ''
            tens = "null"
            if top_matrix.user_by_whom:
                by = top_matrix.user_by_whom.username
            else:
                by = 'Жок'
            if len(str(fol)) == 1:
                str_fol = '0' + str(fol)
            else:
                str_fol = str(fol)
            key_fol = float(str(count) + '.' + str_fol)
            while True:
                if any(d['key'] == key_fol for d in list_matrix):
                    fol += 1
                    if len(str(fol)) == 1:
                        str_fol = '0' + str(fol)
                    else:
                        str_fol = str(fol)
                        if str_fol[-1] == '0':
                            if not any(d['tens'] == (str(count) + '.' + str_fol) for d in list_matrix):
                                tens = str(count) + '.' + str_fol
                        
                    key_fol = float(str(count) + '.' + str_fol)
                    continue
                break
            list_matrix.append(
                {"key": key_fol, "name": top_matrix.username, 'title': top_matrix.user_fullname, "parent": by,
                 "tens": tens})
            if count <= 10:
                if top_matrix.follower1:
                    count += 1
                    check_matrix(top_matrix.follower1.pk, count, 1)
                count -= 1
                if top_matrix.follower2:
                    count += 1
                    check_matrix(top_matrix.follower2.pk, count, 2)
            return

        counter = 0
        check_matrix(self.kwargs['pk'], counter)

        newlist = sorted(list_matrix, key=lambda k: k['key']) 
        context['data'] = newlist
        return context


class UserMatrix13View(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'users/new_matrix13.html'

    def test_func(self):
        return self.request.user.is_authenticated

    def get_context_data(self, **kwargs):
        context = super(UserMatrix13View, self).get_context_data(**kwargs)
        list_matrix = []

        def check_matrix(pk_number, count):
            top_matrix = CustomUser.objects.get(pk=pk_number)
            by = ''
            if top_matrix.user_by_whom:
                by = top_matrix.user_by_whom.id
            else:
                by = 'null'

            if top_matrix.status13:
                status13 = 'Полученно'
            else:
                status13 = 'Не полученно'
            list_matrix.append(
                {"key": top_matrix.id, "name": top_matrix.username, 'title': top_matrix.user_fullname, "parent": by,
                 "comments": f'{status13}'}
            )
            if count <= 10:
                if top_matrix.follower1:
                    count += 1
                    check_matrix(top_matrix.follower1.pk, count)
                count -= 1
                if top_matrix.follower2:
                    count += 1
                    check_matrix(top_matrix.follower2.pk, count)
            return

        counter = 0
        check_matrix(self.kwargs['pk'], counter)
        context['data'] = list_matrix
        return context


class UserListMatrix13(UserPassesTestMixin, DetailView):
    model = CustomUser
    template_name = 'users/matrix13.html'

    def test_func(self):
        return self.request.user.is_authenticated

    def get_context_data(self, **kwargs):
        context = super(UserListMatrix13, self).get_context_data(**kwargs)
        list_matrix = []

        def check_matrix(pk_number, count, fol=0):
            top_matrix = CustomUser.objects.get(pk=pk_number)
            by = ''
            tens = "null"
            status13 = ''
            if top_matrix.user_by_whom:
                by = top_matrix.user_by_whom.username
            else:
                by = 'Жок'
            if top_matrix.status13:
                status13 = 'Полученно'
            else:
                status13 = 'Не полученно'
            if len(str(fol)) == 1:
                str_fol = '0' + str(fol)
            else:
                str_fol = str(fol)
            key_fol = float(str(count) + '.' + str_fol)
            while True:
                if any(d['key'] == key_fol for d in list_matrix):
                    fol += 1
                    if len(str(fol)) == 1:
                        str_fol = '0' + str(fol)
                    else:
                        str_fol = str(fol)
                        if str_fol[-1] == '0':
                            if not any(d['tens'] == (str(count) + '.' + str_fol) for d in list_matrix):
                                tens = str(count) + '.' + str_fol
                        
                    key_fol = float(str(count) + '.' + str_fol)
                    continue
                break
            list_matrix.append(
                {"key": key_fol, "name": top_matrix.username, 'title': top_matrix.user_fullname, "parent": by,
                 "tens": tens, "status13": status13})
            if count <= 10:
                if top_matrix.follower1:
                    count += 1
                    check_matrix(top_matrix.follower1.pk, count, 1)
                count -= 1
                if top_matrix.follower2:
                    count += 1
                    check_matrix(top_matrix.follower2.pk, count, 2)
            return

        counter = 0
        check_matrix(self.kwargs['pk'], counter)

        newlist = sorted(list_matrix, key=lambda k: k['key']) 
        context['data'] = newlist
        return context


class ReferralCreateView(CreateView):
    model = CustomUser
    form_class = ReferralCreateForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'

    def form_valid(self, form):
        user_id = CustomUser.objects.get(pk=self.kwargs['pk'])
        form.instance.user_by_whom = user_id
        return super(ReferralCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ReferralCreateView, self).get_context_data(**kwargs)
        context['user_info'] = CustomUser.objects.get(pk=self.kwargs['pk'])
        return context


def change_password(request, pk):
    user_pass = CustomUser.objects.get(pk=pk)
    user_pass.set_password('bayel123')
    user_pass.save()
    return redirect('users_list')


def user_message_block(request, pk):
    user = CustomUser.objects.get(pk=pk)
    user.user_message = False
    user.save()
    return redirect('user_detail', pk=pk)


# Office model CRUD operations
class OfficeList(UserPassesTestMixin, ListView):
    template_name = 'users/offices_list.html'
    login_url = 'registration/login.html'
    paginate_by = 30

    def get_queryset(self):
        return Office.objects.all()

    def test_func(self):
        return self.request.user.is_superuser


class OfficeDelete(UserPassesTestMixin, DeleteView):
    model = Office
    success_url = reverse_lazy('offices_list')
    template_name = 'users/office_delete.html'

    def test_func(self):
        return self.request.user.is_superuser


class OfficeUpdate(UserPassesTestMixin, UpdateView):
    model = Office
    form_class = OfficeCreateForm
    success_url = reverse_lazy('offices_list')
    template_name = 'users/office_update.html'

    def test_func(self):
        return self.request.user.is_superuser


class OfficeCreate(UserPassesTestMixin, CreateView):
    model = Office
    form_class = OfficeCreateForm
    success_url = reverse_lazy('offices_list')
    template_name = 'users/office_create.html'

    def test_func(self):
        return self.request.user.is_superuser


# Operators model CRUD operations
class OperatorList(UserPassesTestMixin, ListView):
    template_name = 'users/operators/operator_list.html'
    login_url = 'registration/login.html'
    paginate_by = 30

    def get_queryset(self):
        return CustomUser.objects.filter(~Q(username='manager') & Q(is_staff=True) | Q(is_cashier=True)).order_by(
            '-updated')

    def test_func(self):
        return self.request.user.is_staff or self.request.user.is_cashier

    def get_context_data(self, **kwargs):
        context = super(OperatorList, self).get_context_data(**kwargs)
        return context


class OperatorSignUp(UserPassesTestMixin, CreateView):
    form_class = OperatorCreateForm
    success_url = reverse_lazy('operator_list')
    template_name = 'registration/operator_signup.html'

    def test_func(self):
        return self.request.user.is_superuser


class OperatorDetails(UserPassesTestMixin, ListView):
    """Report Search is a view for filtering Users given presents, products and balance. And it
     new registered users. From start date, till end date inclusive."""
    model = CustomUser
    template_name = 'users/operators/operator_details.html'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        """Initializing some variables we will need them later on."""
        self.history_est = []
        self.product_est = []
        self.report_box = {}
        self.report_user = ''

    def test_func(self):
        """This func is checking if user has permission to see this view"""
        return self.request.user.is_superuser

    def get_queryset(self):
        """Here we do all the filterings with the querysets."""
        query = self.request.GET.get('q')
        end_date = self.request.GET.get('end-date')
        """If request has only one 'q' parameter it checks only by the one date."""
        oper = CustomUser.objects.get(pk=self.kwargs['pk'])
        self.report_user = oper
        if end_date == '':
            object_list = OperationHistory.objects.filter(
                Q(oper_his_date__icontains=query) & Q(oper_his_operator=oper) & Q(oper_his_action='status')).order_by(
                '-id')
            history = History.objects.filter(Q(history_operator=oper) & Q(history_date__icontains=query)).order_by(
                '-id')
            product_user = OperationHistory.objects.filter(
                Q(oper_his_operator=oper) & Q(oper_his_action='product') & Q(oper_his_date__icontains=query)).order_by(
                '-id')
            total = 0
            for x in history:
                total = total + x.history_amount

            self.report_box = {'money': total, 'user_counter': object_list.count(),
                               'products': product_user.count()}
            self.history_est = history
            self.product_est = product_user
        elif end_date is None and query is None:
            object_list = OperationHistory.objects.filter(
                Q(oper_his_operator=oper) & Q(oper_his_action='status')).order_by('-id')
            history = History.objects.filter(history_operator=oper).order_by('-id')
            product_user = OperationHistory.objects.filter(
                Q(oper_his_operator=oper) & Q(oper_his_action='product')).order_by('-id')
            total = 0
            for x in history:
                total = total + x.history_amount

            self.report_box = {'money': total, 'user_counter': object_list.count(), 'products': product_user.count()}
            self.history_est = history
            self.product_est = product_user
        else:
            """Otherwise it checks from 'q' date till 'end_date' inclusive."""
            object_list = OperationHistory.objects.filter(Q(oper_his_operator=oper) & Q(oper_his_action='status') & Q(
                oper_his_date__date__range=(query, end_date))).order_by('-id')
            history = History.objects.filter(
                Q(history_date__date__range=(query, end_date)) & Q(history_operator=oper)).order_by('-id')
            product_user = OperationHistory.objects.filter(Q(oper_his_operator=oper) & Q(oper_his_action='product') & Q(
                oper_his_date__date__range=(query, end_date))).order_by('-id')
            total = 0
            for x in history:
                total = total + x.history_amount

            self.report_box = {'money': total, 'user_counter': object_list.count(),
                               'products': product_user.count()}
            self.history_est = history
            self.product_est = product_user
        return object_list

    def get_context_data(self, **kwargs):
        context = super(OperatorDetails, self).get_context_data(**kwargs)
        context['search_input'] = self.request.GET.get('q')
        context['end_date'] = self.request.GET.get('end-date')
        context['history_est'] = self.history_est
        context['product_est'] = self.product_est
        context['report_box'] = self.report_box
        context['report_user'] = self.report_user

        return context


class LoginPageView(LoginView):
    def get_success_url(self):
        us = self.request.user.is_staff
        cash = self.request.user.is_cashier
        if us or cash:
            return reverse('users_list')
        else:
            return reverse('user_detail', args=[self.request.user.pk])


def export_xlwt(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Bayel4000-{date}.xls"'.format(
        date=datetime.now().strftime(
            '%d-%m-%Y'))

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Список участников')
    # Sheet header, first row
    row_num = 0

    ws.col(0).width = 25 * 60
    ws.col(1).width = 25 * 270
    ws.col(2).width = 25 * 300
    ws.col(3).width = 25 * 270
    ws.col(4).width = 25 * 200
    ws.col(5).width = 25 * 270
    ws.col(6).width = 25 * 270
    ws.col(7).width = 25 * 270
    ws.col(8).width = 25 * 270
    ws.col(9).width = 25 * 220
    ws.col(10).width = 25 * 240
    ws.col(11).width = 25 * 240
    ws.col(12).width = 25 * 240
    ws.col(13).width = 25 * 120
    ws.col(14).width = 25 * 120
    ws.col(15).width = 25 * 270
    ws.col(16).width = 25 * 140
    ws.col(17).width = 25 * 270

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.font.colour_index = 4
    font_style.font.height = 300
    ws.row(row_num).height = 800
    font_style.alignment.horz = Alignment.HORZ_CENTER
    font_style.alignment.vert = Alignment.VERT_CENTER
    column = ['Список Участников Программы Бай Эл 4000 (2 и 3 Этап)']
    ws.merge(row_num, row_num, 0, 17)
    ws.write(row_num, 0, column, font_style)
    row_num += 1

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.font.height = 220
    font_style.alignment.horz = Alignment.HORZ_CENTER
    font_style.alignment.vert = Alignment.VERT_CENTER
    pattern = xlwt.Pattern()
    pattern.pattern = Pattern.SOLID_PATTERN
    pattern.pattern_fore_colour = Style.colour_map['gray50']
    font_style.pattern = pattern
    font_style.font.colour_index = 1
    font_style.borders.top = Borders.THIN
    font_style.borders.right = Borders.THIN
    font_style.borders.bottom = Borders.THIN
    font_style.borders.left = Borders.THIN
    ws.row(row_num).height = 400
    columns = ['id', 'Логин', 'ФИО', 'Спонсор', 'Телефон', 'Доступно к снятию 7600', 'Заработанно в 7600 общем ',
               'Доступно к снятию 13000', 'Заработанно в 13000 общем ',
               'Дата регистрации', 'Кол-во подписчиков', 'Подписчик 1', 'Подписчик 2', 'Статус', 'Продукция', 'Белги',
               'Белги Дата', 'Подарочный пользователь', ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    font_style.alignment.horz = Alignment.HORZ_CENTER
    font_style.alignment.vert = Alignment.VERT_CENTER
    rows = CustomUser.objects.filter(is_staff=False).order_by('id') \
        .values_list('id', 'username', 'user_fullname', 'user_by_whom__username', 'user_phone', 'user_balance',
                     'user_total_balance', 'user_balance13',
                     'user_total_balance13', 'user_registration_date', 'followers', 'follower1__username',
                     'follower2__username', 'status', 'product', 'user_notes', 'user_notes_date', 'status_user_gift')

    rows = [[x.strftime("%d-%m-%Y") if isinstance(x, datetime) else x for x in row] for row in rows]
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    row_num += 2

    wb.save(response)
    return response
