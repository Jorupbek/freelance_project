from PIL import Image, ImageFile
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils.datetime_safe import datetime
from django.utils.translation import ugettext_lazy as _
from django.db import models

from image_compress.image_compress import compress_image

ImageFile.LOAD_TRUNCATED_IMAGES = True


class MyValidator(UnicodeUsernameValidator):
    regex = r'^[\w.@+\-]+$'
    message = _('Логин тамга жана сандардан гана турууга тийиш.')


# User office region
class Office(models.Model):
    office_name = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.office_name}'


# Create your models here.
class CustomUser(AbstractUser):
    username_validator = MyValidator()
    slug = models.SlugField(default='', editable=False, max_length=200, null=False)
    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    user_fullname = models.CharField(max_length=255, verbose_name='Аты Жөнү')
    user_image = models.ImageField(verbose_name=_("Колдонуучунун суроту"), upload_to='users/',
                                   default='../static/default_user.png', null=True, blank=True)
    user_passport = models.ImageField(upload_to='users/', default='../static/default.png', null=True, blank=True,
                                      verbose_name='Фотография паспорта')
    user_phone = models.CharField(verbose_name=_("Телефон номери"), max_length=60, null=True,
                                  blank=True)

    # User sponsor
    user_by_whom = models.ForeignKey('self', related_name='users_by', verbose_name=_("Кимден"),
                                     on_delete=models.SET_NULL, null=True,
                                     blank=True)

    # User balance information
    user_balance = models.IntegerField(default=0, verbose_name=_("Алууга мүмкүнчүлүк"))
    user_total_balance = models.IntegerField(default=0, verbose_name='Мен таптым')

    user_balance13_hidden = models.IntegerField(default=0, verbose_name=_("Жашыруун баланс 13"))
    user_balance13 = models.IntegerField(default=0, verbose_name=_("Алууга мүмкүнчүлүк 13"))
    user_total_balance13 = models.IntegerField(default=0, verbose_name=_('Мен таптым 13'))

    # User registration date
    user_registration_date = models.DateTimeField(verbose_name='Дата регистрации', default=datetime.now)

    # User office region
    user_office = models.ForeignKey(Office, on_delete=models.SET_NULL, related_name='user_region', null=True,
                                    blank=True, verbose_name='Офис пользователя')

    # Followers quantity
    followers = models.IntegerField(verbose_name=_('Жолдоочулары'), default=0)
    followers13 = models.IntegerField(verbose_name=_('Жолдоочулары 13'), default=0)
    followers13_passive = models.IntegerField(verbose_name=_('Жашыруун Жолдоочулары 13 '), default=0)

    # First follower
    follower1 = models.ForeignKey('CustomUser', on_delete=models.SET_NULL, related_name='followers_1', null=True,
                                  blank=True, verbose_name='Первый подписчик')
    follower1_date = models.DateTimeField(blank=True, null=True, verbose_name='Дата регистрации 1 подписчика')
    follower1_13_date = models.DateTimeField(blank=True, null=True, verbose_name='Дата регистрации 1 13 подписчика')

    # Second follower
    follower2 = models.ForeignKey('CustomUser', on_delete=models.SET_NULL, related_name='followers_2', null=True,
                                  blank=True, verbose_name='Второй подписчик')
    follower2_date = models.DateTimeField(blank=True, null=True, verbose_name='Дата регистрации 2 подписчика')
    follower2_13_date = models.DateTimeField(blank=True, null=True, verbose_name='Дата регистрации 2 13 подписчика')

    # User registration status
    status = models.BooleanField(verbose_name=_('Статус'), default=False)
    status_date = models.DateTimeField(blank=True, null=True, verbose_name='Дата выдачи статуса')
    status13 = models.BooleanField(verbose_name=_('Статус 13'), default=False)
    status_date13 = models.DateTimeField(blank=True, null=True, verbose_name='Дата выдачи статуса 13')

    # Product and the date when product was given to the user.
    product = models.BooleanField(default=False, verbose_name=_('Продукция'))
    product_given_date = models.DateTimeField(blank=True, null=True, verbose_name='Дата выдачи продукции')
    product13 = models.BooleanField(default=False, verbose_name=_('Продукция 13'))
    product_given_date13 = models.DateTimeField(blank=True, null=True, verbose_name='Дата выдачи продукции 13')

    # terms and conditions
    terms = models.BooleanField(default=False, verbose_name='Политика конфиденциальности')

    # User information to the staff
    user_notes = models.CharField(max_length=155, verbose_name='Заметки', blank=True, null=True)
    user_notes_date = models.DateTimeField(blank=True, null=True, verbose_name='Дата заметки')
    user_notes_user = models.ForeignKey('CustomUser', on_delete=models.SET_NULL, related_name='user_notes_changed',
                                        null=True, blank=True, verbose_name='Оператор заметки')

    # For cashiers
    is_cashier = models.BooleanField(verbose_name='Кассир', default=False)

    # Place in list by updated information
    updating_operator = models.ForeignKey('CustomUser', on_delete=models.SET_NULL, related_name='updated_operator',
                                          blank=True, null=True, verbose_name='Последний оператор')
    # Last time and data when was the model updated
    updated = models.DateTimeField(auto_now=True, verbose_name='Дата обновления')

    # Gift place for users
    status_user_gift = models.BooleanField(default=False, verbose_name='Без денег')
    status_user_gift13 = models.BooleanField(default=False, verbose_name='Первый без денег 13')
    status_user_own = models.BooleanField(default=False, verbose_name='Первый без денег 75')

    # User greetings message
    user_message = models.BooleanField(default=True, verbose_name='Статус приветственного сообщение ')

    # User status for moving to third tour
    user_second_tour = models.BooleanField(default=False, verbose_name='Статус готовности перехода на 3 уровень')

    # User has a status for third tour
    user_second_status = models.BooleanField(default=False, verbose_name='Статус получения 3 уровня')

    def __str__(self):
        return f'{self.username}'


class UserFollowers(models.Model):
    top_user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='top_user_info')
    follower_user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='follower_user_info')
    follower_date = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return f'{self.top_user}'


class OperationHistory(models.Model):
    oper_his_operator = models.ForeignKey(CustomUser, on_delete=models.CASCADE, blank=True, null=True)
    oper_his_user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='oper_his_user_info')
    oper_his_action = models.CharField(max_length=50)
    oper_his_note = models.CharField(max_length=255)
    oper_his_date = models.DateTimeField(default=datetime.now)
    oper_his_amount = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.oper_his_operator} ---- {self.oper_his_action} ---- {self.oper_his_user}'


@receiver(post_save, sender=CustomUser)
def registration(sender, instance, created, **kwargs):
    if instance.status and instance.status_date is None and not instance.is_cashier and not instance.is_staff:
        def date_legs(user_instance):
            # Save date and time, when user got the approval for status
            main_user = CustomUser.objects.get(id=user_instance.id)
            main_user.status_date = datetime.now()
            main_user.save()

            # Saving instance user to one of the legs of his sponsor
            if user_instance.user_by_whom:
                first_sponsor = CustomUser.objects.get(id=user_instance.user_by_whom.id)
                if not first_sponsor.follower1:
                    first_sponsor.follower1 = user_instance
                    first_sponsor.follower1_date = datetime.now()
                elif not first_sponsor.follower2:
                    first_sponsor.follower2 = user_instance
                    first_sponsor.follower2_date = datetime.now()
                first_sponsor.save()

        def money_transfer(first_leg, count, amount, first_user):
            sponsor = CustomUser.objects.get(id=first_leg.user_by_whom.id)
            sponsor.user_balance += amount
            sponsor.user_total_balance += amount
            sponsor.followers += 1
            UserFollowers.objects.create(top_user=sponsor, follower_user=first_user, follower_date=datetime.now())
            if amount != 0:
                OperationHistory.objects.create(oper_his_operator=first_user,
                                                oper_his_user=sponsor, oper_his_amount=amount,
                                                oper_his_note=f'Получил(а) наличные за подписчика {first_user.user_fullname}',
                                                oper_his_action='money')
            sponsor.save()
            count += 1
            if count <= 10 and first_user.status_user_gift and sponsor.user_by_whom:
                money_transfer(sponsor, count, 0, first_user)
            elif count <= 10 and not instance.status_user_gift:
                if (count == 1 or count == 3 or (5 <= count <= 9)) and sponsor.user_by_whom:
                    money_transfer(sponsor, count, 500, first_user)
                elif (count == 2 or count == 4) and sponsor.user_by_whom:
                    money_transfer(sponsor, count, 250, first_user)

        date_legs(instance)
        if instance.user_by_whom:
            if instance.status_user_gift or instance.status_user_own:
                money_transfer(instance, 0, 0, instance)
            else:
                money_transfer(instance, 0, 500, instance)

    if instance.status13 and instance.status_date13 is None and not instance.is_cashier and not instance.is_staff:
        def status_date13(user_instance):
            first = CustomUser.objects.get(id=user_instance.id)
            first.status_date13 = datetime.now()
            first.user_balance13 += first.user_balance13_hidden
            first.user_total_balance13 += first.user_balance13_hidden
            first.followers13 += first.followers13_passive
            OperationHistory.objects.create(oper_his_operator=first,
                                            oper_his_user=first, oper_his_amount=first.user_balance13_hidden,
                                            oper_his_note=f'Получил(а) наличные за 3 уровень за '
                                                          f'{first.followers13_passive} подписчиков',
                                            oper_his_action='money')
            first.save()

        def money_for_13(account, count, amount, first_user):
            sponsor = CustomUser.objects.get(id=account.user_by_whom.id)
            if sponsor.status13:
                sponsor.user_total_balance13 += amount
                sponsor.user_balance13 += amount
                sponsor.followers13 += 1
                if amount != 0:
                    OperationHistory.objects.create(oper_his_operator=first_user,
                                                    oper_his_user=sponsor, oper_his_amount=amount,
                                                    oper_his_note=f'Получил(а) наличные за 3 этап от подписчика '
                                                                  f'{first_user.user_fullname}',
                                                    oper_his_action='money')
            else:
                sponsor.user_balance13_hidden += amount
                sponsor.followers13_passive += 1

            sponsor.save()
            count += 1
            if sponsor.user_by_whom:
                if count <= 2:
                    money_for_13(sponsor, count, 500, first_user)
                elif count <= 10:
                    money_for_13(sponsor, count, 1000, first_user)

        status_date13(instance)
        if instance.user_by_whom:
            if instance.status_user_gift13:
                money_for_13(instance, 1, 0, instance)
            else:
                money_for_13(instance, 1, 500, instance)


@receiver(pre_save, sender=CustomUser)
def do_something_if_changed(sender, instance, **kwargs):
    try:
        obj = sender.objects.get(pk=instance.pk)
    except sender.DoesNotExist:
        pass
    else:
        if not obj.user_notes == instance.user_notes:
            action = f'Поменял Белги c "{obj.user_notes}" на "{instance.user_notes}"'
            OperationHistory.objects.create(oper_his_operator=instance.updating_operator, oper_his_user=instance,
                                            oper_his_note=action, oper_his_action='belgi')
        if not obj.product == instance.product:
            if obj.product:
                action = 'Снял метку с продукции'
            else:
                action = 'Выдал продукцию'
            OperationHistory.objects.create(oper_his_operator=instance.updating_operator, oper_his_user=instance,
                                            oper_his_note=action, oper_his_action='product')
        if not obj.status == instance.status:
            OperationHistory.objects.create(oper_his_operator=instance.updating_operator, oper_his_user=instance,
                                            oper_his_note='Подтвердил статус', oper_his_action='status')

        if obj.user_image != instance.user_image:
            instance.user_image = compress_image(instance.user_image)

        if obj.user_passport != instance.user_passport:
            instance.user_passport = compress_image(instance.user_passport)
