from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser, Office
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator


class CustomUserCreationForm(UserCreationForm):
    username = forms.CharField(label="Логин", error_messages={'required': _(u"Бул тилке бош")}, required=True,
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control', 'placeholder': 'Логин'}))
    user_fullname = forms.CharField(label=_(u'Аты Жөнү'), error_messages={'required': _(u"Бул тилке бош")},
                                    required=True,
                                    widget=forms.TextInput(
                                        attrs={'class': 'form-control', 'placeholder': _(u'Аты Жөнү')}))
    user_phone = forms.CharField(label="Телефон", error_messages={'required': _(u"Бул тилке бош")}, required=True,
                                 widget=forms.TextInput(
                                     attrs={'class': 'form-control', 'placeholder': _(u'Телефон номери')}))
    user_by_whom = forms.ModelChoiceField(label=_(u'Жетекчи'), required=False,
                                          queryset=CustomUser.objects.filter(is_staff=False,
                                                                             follower2=None),
                                          empty_label=_(u"Кимден"),
                                          widget=forms.Select(attrs={'class': 'form-control ', 'data-plugin': 'select2',
                                                                     'data-minimum-input-length': '1'}))
    user_office = forms.ModelChoiceField(label=_(u'Регион'), required=True,
                                         queryset=Office.objects.all(),
                                         empty_label=_(u"Регион"),
                                         widget=forms.Select(attrs={'class': 'form-control my-form'}))
    password1 = forms.CharField(label=_(u'Сыр сөз'), error_messages={'required': _(u"Бул тилке бош")}, required=True,
                                min_length=8, validators=[
            RegexValidator('^(\w+\d+|\d+\w+)+$',
                           message=_(u'Сырсөз англис тамга жана сандардан жыйындысы болуп саналат'))],
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control',
                                           'placeholder': _(u'Сыр сөз'), 'value': 'bayel123'}), )
    password2 = forms.CharField(label=_(u'Сыр сөздү кайталаңыз'), error_messages={'required': _(u"Бул тилке бош")},
                                required=True,
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control', 'placeholder': _(u'Сыр сөздү кайталаңыз'),
                                           'value': 'bayel123'}))
    terms = forms.BooleanField(label=_(u'Мен "Бай Эл" программасынын  Купуялык саясатын окуп чыгып, кабыл алып жатам'),
                               error_messages={'required': _(u"Бул тилке бош")}, required=True,
                               widget=forms.CheckboxInput(
                                   attrs={'class': 'form-control terms',
                                          'onclick': "showMe('autoUpdate', 'picture-signup')",
                                          'checked': 'true'}), )

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = UserCreationForm.Meta.fields + (
            'username', 'user_fullname', 'user_phone', 'user_by_whom', 'user_office',)

    def clean_username(self):
        username = self.cleaned_data['username']
        r = CustomUser.objects.filter(username=username)
        if r.count():
            raise ValidationError("Логин уже используется, пожалуйста введите другой логин")
        return username

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("Пароли не совпадают")
        return password2


class CustomUserChangeForm(UserChangeForm):
    def __init__(self, *args, **kwargs):
        super(CustomUserChangeForm, self).__init__(*args, **kwargs)
        self.fields['user_by_whom'].queryset = CustomUser.objects.filter(is_staff=False)
        password = forms.CharField(label=False)

    username = forms.CharField(label="Логин", error_messages={'required': _(u"Бул тилке бош")}, required=True,
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control', 'placeholder': 'Логин'}))
    user_fullname = forms.CharField(label=_(u'Аты Жөнү'), error_messages={'required': _(u"Бул тилке бош")},
                                    required=True,
                                    widget=forms.TextInput(
                                        attrs={'class': 'form-control', 'placeholder': _(u'Аты Жөнү')}))
    user_notes = forms.CharField(label=_(u'Белги'), required=False,
                                 widget=forms.TextInput(
                                     attrs={'class': 'form-control', 'placeholder': _(u'Белги')}))
    user_office = forms.ModelChoiceField(label=_(u'Регион'), required=True,
                                         queryset=Office.objects.all(),
                                         empty_label=_(u"Регион"),
                                         widget=forms.Select(attrs={'class': 'form-control my-form'}))
    user_by_whom = forms.ModelChoiceField(label=_(u'Жетекчи'), required=False,
                                          queryset=CustomUser.objects.filter(is_staff=False,
                                                                             follower2=None),
                                          empty_label=_(u"Кимден"),
                                          widget=forms.Select(attrs={'class': 'form-control ', 'data-plugin': 'select2',
                                                                     'data-minimum-input-length': '1'}))
    user_phone = forms.CharField(label="Телефон", error_messages={'required': _(u"Бул тилке бош")}, required=True,
                                 widget=forms.TextInput(
                                     attrs={'class': 'form-control', 'placeholder': _(u'Телефон')}))
    user_balance = forms.CharField(label="Доступно для снятия", error_messages={'required': _(u"Бул тилке бош")},
                                   required=True,
                                   widget=forms.TextInput(
                                       attrs={'class': 'form-control', 'placeholder': _(u'Доступно для снятия')}))
    user_total_balance = forms.CharField(label="Всего заработанно", error_messages={'required': _(u"Бул тилке бош")},
                                         required=True,
                                         widget=forms.TextInput(
                                             attrs={'class': 'form-control', 'placeholder': _(u'Всего заработанно')}))
    user_balance13 = forms.CharField(label="Доступно для снятия 13", error_messages={'required': _(u"Бул тилке бош")},
                                     required=True,
                                     widget=forms.TextInput(
                                         attrs={'class': 'form-control', 'placeholder': _(u'Доступно для снятия 13')}))
    user_total_balance13 = forms.CharField(label="Всего заработанно 13",
                                           error_messages={'required': _(u"Бул тилке бош")},
                                           required=True,
                                           widget=forms.TextInput(
                                               attrs={'class': 'form-control',
                                                      'placeholder': _(u'Всего заработанно 13')}))
    user_balance13_hidden = forms.CharField(label="Скрыто 13",
                                            error_messages={'required': _(u"Бул тилке бош")},
                                            required=True,
                                            widget=forms.TextInput(
                                                attrs={'class': 'form-control',
                                                       'placeholder': _(u'Скрыто 13')}))
    followers = forms.CharField(label="Подписчики", error_messages={'required': _(u"Бул тилке бош")},
                                required=True,
                                widget=forms.TextInput(
                                    attrs={'class': 'form-control', 'placeholder': _(u'Подписчики')}))
    followers13 = forms.CharField(label="Подписчики 13", error_messages={'required': _(u"Бул тилке бош")},
                                  required=True,
                                  widget=forms.TextInput(
                                      attrs={'class': 'form-control', 'placeholder': _(u'Подписчики 13')}))
    followers13_passive = forms.CharField(label="Подписчики скрытые 13",
                                          error_messages={'required': _(u"Бул тилке бош")},
                                          required=True,
                                          widget=forms.TextInput(
                                              attrs={'class': 'form-control',
                                                     'placeholder': _(u'Подписчики скрытые 13')}))

    class Meta:
        model = CustomUser
        fields = UserCreationForm.Meta.fields + (
            'username', 'user_fullname', 'user_image', 'user_by_whom', 'user_phone', 'user_balance13',
            'user_total_balance13', 'status_user_own', 'product13', 'followers13',
            'user_balance', 'user_total_balance', 'followers', 'status', 'status13', 'user_notes', 'status_user_gift13',
            'is_staff', 'is_cashier', 'status_user_gift', 'user_office', 'product', 'user_message',
            'user_second_status', 'user_balance13_hidden', 'followers13_passive'

        )


class ReferralCreateForm(UserCreationForm):
    username = forms.CharField(label="ФИО", error_messages={'required': _(u"Бул тилке бош")}, required=True,
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control', 'placeholder': 'ФИО'}))
    user_phone = forms.CharField(label="Телефон", error_messages={'required': _(u"Бул тилке бош")}, required=True,
                                 widget=forms.TextInput(
                                     attrs={'class': 'form-control', 'placeholder': _(u'Телефон номери')}))
    user_office = forms.ModelChoiceField(label=_(u'Регион'), required=True,
                                         queryset=Office.objects.all(),
                                         empty_label=_(u"Регион"),
                                         widget=forms.Select(attrs={'class': 'form-control '}))
    password1 = forms.CharField(label=_(u"Сыр сөз"), error_messages={'required': _(u"Бул тилке бош")}, required=True,
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control', 'placeholder': _(u'Сыр сөз')}), )
    password2 = forms.CharField(label=_(u"Сыр сөздү кайталаңыз"), error_messages={'required': _(u"Бул тилке бош")},
                                required=True,
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control', 'placeholder': _(u'Сыр сөздү кайталаңыз')}))
    terms = forms.BooleanField(label='Я прочитал сооглашение компании "Бай Эл"',
                               error_messages={'required': _(u"Бул тилке бош")}, required=True,
                               widget=forms.CheckboxInput(
                                   attrs={'class': 'form-control terms'}), )

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = UserCreationForm.Meta.fields + ('username', 'user_phone', 'user_office',)

    def clean_username(self):
        username = self.cleaned_data['username']
        r = CustomUser.objects.filter(username=username)
        if r.count():
            raise ValidationError("Логин уже используется, пожалуйста введите другой логин")
        return username

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("Пароли не совпадают")
        return password2


# Office Creation form
class OfficeCreateForm(forms.ModelForm):
    office_name = forms.CharField(label=_(u'Название офиса'), error_messages={'required': _(u"Бул тилке бош")},
                                  required=True,
                                  widget=forms.TextInput(
                                      attrs={'class': 'form-control', 'placeholder': _(u'Название офиса')}))

    class Meta:
        model = Office
        fields = ['office_name']


class OperatorCreateForm(UserCreationForm):
    username = forms.CharField(label="Логин", error_messages={'required': _(u"Бул тилке бош")}, required=True,
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control', 'placeholder': 'Логин'}))
    user_fullname = forms.CharField(label=_(u'Аты Жөнү'), error_messages={'required': _(u"Бул тилке бош")},
                                    required=True,
                                    widget=forms.TextInput(
                                        attrs={'class': 'form-control', 'placeholder': _(u'Аты Жөнү')}))
    user_phone = forms.CharField(label="Телефон", error_messages={'required': _(u"Бул тилке бош")}, required=True,
                                 widget=forms.TextInput(
                                     attrs={'class': 'form-control', 'placeholder': _(u'Телефон номери')}))
    user_office = forms.ModelChoiceField(label=_(u'Регион'), required=True,
                                         queryset=Office.objects.all(),
                                         empty_label=_(u"Регион"),
                                         widget=forms.Select(attrs={'class': 'form-control '}))
    password1 = forms.CharField(label=_(u"Сыр сөз"), error_messages={'required': _(u"Бул тилке бош")}, required=True,
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control'}), )
    password2 = forms.CharField(label=_(u"Сыр сөздү кайталаңыз"), error_messages={'required': _(u"Бул тилке бош")},
                                required=True,
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control'}))

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = UserCreationForm.Meta.fields + (
            'username', 'user_fullname', 'user_phone', 'user_office', 'is_staff', 'is_cashier')

    def clean_username(self):
        username = self.cleaned_data['username']
        r = CustomUser.objects.filter(username=username)
        if r.count():
            raise ValidationError("Логин уже используется, пожалуйста введите другой логин")
        return username

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("Пароли не совпадают")
        return password2
