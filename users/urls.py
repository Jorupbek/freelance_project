from django.urls import path

from .views import *

urlpatterns = [
    path('signup/', SignUpView.as_view(), name='signup'),
    path('list75/', UserList75.as_view(), name='users_list75'),
    path('list/', UserList.as_view(), name='users_list'),
    path('users_second_tour/', UsersSecondTour.as_view(), name='users_second_tour'),
    path('user_second_update/<int:pk>/<int:oper>/', UserSecondUpdate.as_view(), name='user_second_update'),
    path('users_reg/', UserRegTodayList.as_view(), name='users_today'),
    path('detail/<int:pk>/', UserDetail.as_view(), name='user_detail'),
    path('balance/<int:pk>/', UserBalance.as_view(), name='user_balance'),
    path('update/<int:pk>/<int:oper>/', UserUpdate.as_view(), name='user_update'),
    path('belgi_update/<int:pk>/<int:oper>/', BelgiUpdates.as_view(), name='belgi_update'),
    path('delete/<int:pk>/', UserDelete.as_view(), name='user_delete'),
    path('matrix_list/<int:pk>/', UserListMatrix.as_view(), name='matrix_list'),
    path('matrix/<int:pk>/', UserMatrixView.as_view(), name='user_matrix'),
    path('matrix13_list/<int:pk>/', UserListMatrix13.as_view(), name="matrix13_list"),
    path('matrix13/<int:pk>/', UserMatrix13View.as_view(), name='user_matrix13'),
    path('upload/<int:pk>/', PassportUploadView.as_view(), name='upload'),
    path('referral/<int:pk>/', ReferralCreateView.as_view(), name='referral_create'),
    path('changepswd/<int:pk>/', change_password, name='change_pswd'),
    path('signup/confirm/', SignUpConfirmPage.as_view(), name='confirm'),
    path('users_wait/', UserWaitList.as_view(), name='users_wait'),
    # Office urls
    path('offices_list/', OfficeList.as_view(), name='offices_list'),
    path('office_delete/<int:pk>/', OfficeDelete.as_view(), name='office_delete'),
    path('office_update/<int:pk>/', OfficeUpdate.as_view(), name='office_update'),
    path('office_create/', OfficeCreate.as_view(), name='office_create'),
    # Operator urls
    path('operator_list/', OperatorList.as_view(), name='operator_list'),
    path('operator_signup/', OperatorSignUp.as_view(), name='operator_signup'),
    path('operator_details/<int:pk>/', OperatorDetails.as_view(), name='operator_details'),
    # Login Page view
    path('login/', LoginPageView.as_view(), name='login'),
    path('user_message_block/<int:pk>/', user_message_block, name='user_message_block'),
    # Export xlwt all users
    path('export_xlwt/', export_xlwt, name='export_xlwt'),
]
