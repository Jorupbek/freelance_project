from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from image_compress.image_compress import compress_image


class News(models.Model):
    title = models.CharField(max_length=255, verbose_name='Заголовок')
    title_ru = models.CharField(max_length=255, verbose_name='Заголовок на русском')
    description = models.TextField(verbose_name='Описание')
    description_ru = models.TextField(verbose_name='Описание на русском')
    date = models.DateField(auto_now_add=True)
    image = models.ImageField(upload_to='news', default='../static/default.png', blank=True, null=True,
                              verbose_name="Изображение")

    def __str__(self):
        return self.title


@receiver(pre_save, sender=News)
def handle_image_compression(sender, instance, **kwargs):
    try:
        post_obj = News.objects.get(pk=instance.pk)
    except News.DoesNotExist:
        # the object does not exists, so compress the image
        instance.image = compress_image(instance.image)
    else:
        # the object exists, so check if the image field is updated
        if post_obj.image != instance.image:
            instance.image = compress_image(instance.image)
