from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import News


class CreateNewsForm(forms.ModelForm):
    title = forms.CharField(label="Аталышы", error_messages={'required': _(u"Бул тилке бош")}, required=True,
                            widget=forms.TextInput(
                                attrs={'class': 'form-control', 'placeholder': 'Аталышы'}))
    title_ru = forms.CharField(label="Заголовок на русском", error_messages={'required': _(u"Бул тилке бош")},
                               required=True,
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control'}))
    description = forms.CharField(label="Баяны", error_messages={'required': _(u"Бул тилке бош")}, required=True,
                                  widget=forms.Textarea(
                                      attrs={'class': 'form-control', 'placeholder': 'Баяны'}))
    description_ru = forms.CharField(label="Описание на русском", error_messages={'required': _(u"Бул тилке бош")},
                                     required=True,
                                     widget=forms.Textarea(
                                         attrs={'class': 'form-control', 'placeholder': 'Описание на русском'}))

    class Meta:
        model = News
        fields = ['title', 'title_ru', 'description', 'description_ru', 'image']


# class UpdateNewsForm(forms.ModelForm):
    # title = forms.CharField(label="Аталышы", error_messages={'required': _(u"Бул тилке бош")}, required=True,
    #                         widget=forms.TextInput(
    #                             attrs={'class': 'form-control', 'placeholder': 'Аталышы'}))
    # title_ru = forms.CharField(label="Заголовок на русском", error_messages={'required': _(u"Бул тилке бош")},
    #                            required=True,
    #                            widget=forms.TextInput(
    #                                attrs={'class': 'form-control'}))
    # description = forms.CharField(label="Баяны", error_messages={'required': _(u"Бул тилке бош")}, required=True,
    #                               widget=forms.Textarea(
    #                                   attrs={'class': 'form-control', 'placeholder': 'Баяны'}))
    # description_ru = forms.CharField(label="Описание на русском", error_messages={'required': _(u"Бул тилке бош")},
    #                                  required=True,
    #                                  widget=forms.Textarea(
    #                                      attrs={'class': 'form-control', 'placeholder': 'Описание на русском'}))
    #
    # class Meta:
    #     model = News
    #     fields = ['title', 'title_ru', 'description', 'description_ru', 'image']
