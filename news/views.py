from django.shortcuts import render
from django.contrib.auth.mixins import UserPassesTestMixin
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import News
from .forms import CreateNewsForm


class NewsList(ListView):
    model = News
    template_name = 'news/news_list.html'
    ordering = ['-id']


class NewsDetail(DetailView):
    model = News
    template_name = 'news/news_detail.html'


class NewsManagerList(UserPassesTestMixin, ListView):
    model = News
    template_name = 'news/news_manager_list.html'

    def test_func(self):
        return self.request.user.is_superuser


class NewsCreateView(UserPassesTestMixin, CreateView):
    model = News
    template_name = 'news/create_news.html'
    form_class = CreateNewsForm
    success_url = reverse_lazy('news_manager')

    def test_func(self):
        return self.request.user.is_superuser


class NewsUpdateView(UserPassesTestMixin, UpdateView):
    model = News
    template_name = 'news/update_news.html'
    form_class = CreateNewsForm
    success_url = reverse_lazy('news_manager')

    def test_func(self):
        return self.request.user.is_superuser


class NewsDeleteView(UserPassesTestMixin, DeleteView):
    model = News
    template_name = 'news/delete_news.html'
    success_url = reverse_lazy('news_manager')

    def test_func(self):
        return self.request.user.is_superuser
