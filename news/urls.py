from django.urls import path
from .views import NewsList, NewsDetail, NewsCreateView, NewsManagerList, NewsUpdateView, NewsDeleteView

urlpatterns = [
    path('', NewsList.as_view(), name='news_list'),
    path('detail/<int:pk>/', NewsDetail.as_view(), name='news_detail'),
    path('create/', NewsCreateView.as_view(), name='news_create'),
    path('update/<int:pk>/', NewsUpdateView.as_view(), name='news_update'),
    path('manager/', NewsManagerList.as_view(), name='news_manager'),
    path('delete/<int:pk>/', NewsDeleteView.as_view(), name='news_delete'),
]
