from .models import News


def news_context(request):
    news = News.objects.all()
    return {'news': news}
