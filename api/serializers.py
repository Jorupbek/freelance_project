from rest_auth.serializers import TokenSerializer
from rest_framework import serializers

from news.models import News
from pages.models import Resources
from users.models import CustomUser
from balance.models import History


class UsersSerializer(serializers.ModelSerializer):
    manager = serializers.CharField(source='by_whom.user_fullname', default=None)
    follow1_75 = serializers.CharField(source='follower1_75.user_fullname', default=None)
    follow2_75 = serializers.CharField(source='follower2_75.user_fullname', default=None)
    registration_date = serializers.DateTimeField(format="%d-%m-%Y", required=False, read_only=True)
    registration_date_13 = serializers.DateTimeField(format="%d-%m-%Y", required=False, read_only=True)

    class Meta:
        model = CustomUser
        fields = (
            'id', 'username', 'user_fullname', 'password', 'user_image', 'manager', 'phone_number', 'enter_balance',
            'balance', 'total_balance', 'balance13', 'total_balance13', 'followers', 'followers13', 'product',
            'status75', 'status13', 'notes', 'follow1_75', 'follow2_75', 'registration_date', 'registration_date_13')


class HistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = History
        fields = ('user_id', 'history_amount', 'history_remark', 'history_date')


class ResourcesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resources
        fields = ('id', 'file', 'title')


class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = ('id', 'title', 'title_ru', 'description', 'description_ru', 'date', 'image')


class CustomTokenSerializer(TokenSerializer):
    user = UsersSerializer(read_only=True)

    class Meta(TokenSerializer.Meta):
        fields = ('key', 'user')
