from django.db.models import Q
from rest_framework.permissions import IsAdminUser

from news.models import News
from pages.models import Resources
from .serializers import UsersSerializer, HistorySerializer, ResourcesSerializer, NewsSerializer
from django.contrib.auth import get_user_model
from rest_framework import viewsets, generics
from balance.models import History


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAdminUser,)
    serializer_class = UsersSerializer

    def get_queryset(self, *args, **kwargs):
        queryset_list = get_user_model().objects.all()
        query_username = self.request.GET.get("username")
        if query_username:
            queryset_list = queryset_list.filter(
                Q(username__exact=query_username)
            ).distinct()
        query_id = self.request.GET.get("id")
        if query_id:
            queryset_list = queryset_list.filter(
                Q(id__exact=query_id)
            ).distinct()
        return queryset_list


class HistoryList(viewsets.ModelViewSet):
    permission_classes = (IsAdminUser,)
    queryset = History.objects.order_by('-id')
    serializer_class = HistorySerializer


class ResourcesList(viewsets.ModelViewSet):
    permission_classes = (IsAdminUser,)
    queryset = Resources.objects.order_by('-id')
    serializer_class = ResourcesSerializer


class NewsList(viewsets.ModelViewSet):
    permission_classes = (IsAdminUser,)
    queryset = News.objects.order_by('-id')
    serializer_class = NewsSerializer


