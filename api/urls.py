from rest_framework.routers import SimpleRouter
from .views import UserViewSet, HistoryList, ResourcesList, NewsList

router = SimpleRouter()

router.register('users', UserViewSet, 'users')
router.register('history', HistoryList, 'history')
router.register('resources', ResourcesList, 'resources')
router.register('news', NewsList, 'news')

urlpatterns = router.urls
