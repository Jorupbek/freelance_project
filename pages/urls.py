from django.urls import path
from .views import HomePageView, ProfilePageView, StaticUpdateView, ResourceCreateView, ResourceDeleteView, \
    ResourceListView, ReportPageView

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('profile/', ProfilePageView.as_view(), name='profile'),
    path('statistic/update/<int:pk>/', StaticUpdateView.as_view(), name='statistic'),
    path('resource/', ResourceListView.as_view(), name='resource'),
    path('resource/create', ResourceCreateView.as_view(), name='resource_create'),
    path('resource/delete/<int:pk>', ResourceDeleteView.as_view(), name='resource_delete'),
    path('report/', ReportPageView.as_view(), name='report'),
]
