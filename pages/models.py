from django.db import models
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class Statistics(models.Model):
    registration_users = models.IntegerField(verbose_name=_('Катталган адамдар'))
    presents = models.IntegerField(verbose_name=_('Алынган белектер'))
    starts = models.IntegerField(verbose_name=_('Ийгилик ээлери'))


class Resources(models.Model):
    title = models.CharField(verbose_name='Название файла', max_length=255)
    file = models.FileField(verbose_name='Файл')

    def __str__(self):
        return f'{self.title}'
