from django.contrib.auth.mixins import UserPassesTestMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView, UpdateView, CreateView, DeleteView
from django.db.models import Q
from pip._vendor import requests

from balance.models import History, History13
from users.models import CustomUser, OperationHistory, Office
from .models import Statistics, Resources
from django.views.generic import View


class ReportPageView(TemplateView):
    template_name = 'pages/report.html'


class ReportSearchView(UserPassesTestMixin, ListView):
    """Report Search is a view for filtering Users given presents, products and balance. And it
     new registered users. From start date, till end date inclusive."""
    model = CustomUser
    template_name = 'pages/report_search.html'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        """Initializing some variables we will need them later on."""
        self.history_est = []
        self.history_est13 = []
        self.product_est = []
        self.report_box = {}

    def test_func(self):
        """This func is checking if user has permission to see this view"""
        return self.request.user.is_staff or self.request.user.is_cashier

    def get_queryset(self):
        """Here we do all the filterings with the querysets."""
        query = self.request.GET.get('q')
        end_date = self.request.GET.get('end-date')
        user_office = self.request.GET.get('user_office')
        """If request has only one 'q' parameter it checks only by the one date."""
        if user_office == '':
            if end_date == '':
                object_list = CustomUser.objects.filter(user_registration_date__icontains=query, is_staff=False,
                                                        is_cashier=False).order_by('-id')
                history = History.objects.filter(history_date__contains=query).order_by('-id')
                history13 = History13.objects.filter(history_date__contains=query).order_by('-id')
                product_user = OperationHistory.objects.filter(
                    Q(oper_his_date__contains=query) & Q(oper_his_action='product')).order_by('-id')
                total = 0
                for x in history:
                    total = total + x.history_amount
                total13 = 0
                for x in history13:
                    total13 += x.history_amount13

                self.report_box = {'money': total, 'money13': total13, 'user_counter': object_list.count(),
                                   'products': product_user.count()}
                self.history_est = history
                self.history_est13 = history13
                self.product_est = product_user
            else:
                """Otherwise it checks from 'q' date till 'end_date' inclusive."""
                object_list = CustomUser.objects.filter(user_registration_date__date__range=(query, end_date),
                                                        is_cashier=False, is_staff=False).order_by(
                    '-id')
                history = History.objects.filter(history_date__date__range=(query, end_date)).order_by('-id')
                history13 = History13.objects.filter(history_date__date__range=(query, end_date)).order_by('-id')
                product_user = OperationHistory.objects.filter(
                    Q(oper_his_date__date__range=(query, end_date)) & Q(oper_his_action='product')).order_by('-id')
                total = 0
                for x in history:
                    total = total + x.history_amount
                total13 = 0
                for x in history13:
                    total13 += x.history_amount13
                self.report_box = {'money': total, 'money13': total13, 'user_counter': object_list.count(),
                                   'products': product_user.count()}
                self.history_est = history
                self.history_est13 = history13
                self.product_est = product_user
        else:
            if end_date == '':
                object_list = CustomUser.objects.filter(
                    Q(user_registration_date__icontains=query) & Q(user_office__office_name=user_office) & Q(
                        is_cashier=False, is_staff=False)).order_by(
                    '-id')
                history = History.objects.filter(Q(history_date__icontains=query) & Q(
                    history_office=user_office)).order_by('-id')
                history13 = History13.objects.filter(Q(history_date__icontains=query) & Q(
                    history_office13=user_office)).order_by('-id')
                product_user = OperationHistory.objects.filter(
                    Q(oper_his_date__icontains=query) & Q(oper_his_action='product') & Q(
                        oper_his_user__user_office__office_name=user_office)).order_by('-id')
                total = 0
                for x in history:
                    total = total + x.history_amount
                total13 = 0
                for x in history13:
                    total13 += x.history_amount13
                self.report_box = {'money': total, 'money13': total13, 'user_counter': object_list.count(),
                                   'products': product_user.count()}
                self.history_est = history
                self.history_est13 = history13
                self.product_est = product_user
            else:
                """Otherwise it checks from 'q' date till 'end_date' inclusive."""
                object_list = CustomUser.objects.filter(
                    Q(user_registration_date__date__range=(query, end_date)) & Q(
                        user_office__office_name=user_office) & Q(is_staff=False, is_cashier=False)).order_by(
                    '-id')
                history = History.objects.filter(Q(history_date__date__range=(query, end_date)) &
                                                 Q(history_office=user_office)).order_by('-id')
                history13 = History13.objects.filter(Q(history_date__date__range=(query, end_date)) &
                                                     Q(history_office13=user_office)).order_by('-id')
                product_user = OperationHistory.objects.filter(
                    Q(oper_his_date__date__range=(query, end_date)) & Q(oper_his_action='product') &
                    Q(oper_his_user__user_office__office_name=user_office)).order_by('-id')
                total = 0
                for x in history:
                    total = total + x.history_amount
                total13 = 0
                for x in history13:
                    total13 += x.history_amount13
                self.report_box = {'money': total, 'money13': total13, 'user_counter': object_list.count(),
                                   'products': product_user.count()}
                self.history_est = history
                self.history_est13 = history13
                self.product_est = product_user

        return object_list

    def get_context_data(self, **kwargs):
        context = super(ReportSearchView, self).get_context_data(**kwargs)
        context['search_input'] = self.request.GET.get('q')
        context['end_date'] = self.request.GET.get('end-date')
        if self.request.GET.get('user_office'):
            office = Office.objects.get(office_name__contains=self.request.GET.get('user_office'))
        else:
            office = self.request.GET.get('user_office')
        context['user_office_context'] = office
        context['product_est'] = self.product_est
        context['history_est'] = self.history_est
        context['history_est13'] = self.history_est13
        context['report_box'] = self.report_box

        return context


class HomePageView(TemplateView):
    template_name = 'pages/home.html'


class ProfilePageView(TemplateView):
    template_name = 'pages/profile.html'


class StaticListView(ListView):
    model = Statistics
    template_name = 'pages/home.html'


class StaticUpdateView(UpdateView):
    model = Statistics
    template_name = 'pages/statistic_update.html'
    fields = '__all__'
    success_url = reverse_lazy('home')


def my_404(request, exception):
    return render(request, 'errors/404.html', locals())


def my_403(request, exception):
    return render(request, 'errors/403.html', locals())


def my_400(request, exception):
    return render(request, 'errors/400.html', locals())


def my_503(request, exception):
    return render(request, 'errors/503.html', locals())


class ResourceListView(ListView):
    model = Resources
    template_name = 'pages/resource.html'


class ResourceCreateView(UserPassesTestMixin, CreateView):
    model = Resources
    template_name = 'pages/resource_create.html'
    success_url = reverse_lazy('resource')
    fields = '__all__'

    def test_func(self):
        return self.request.user.is_staff


class ResourceDeleteView(UserPassesTestMixin, DeleteView):
    model = Resources
    template_name = 'pages/resource_delete.html'
    success_url = reverse_lazy('resource')

    def test_func(self):
        return self.request.user.is_staff


class SendFormTelegram(View):

    def get(self, request):
        # Get the form data
        name = request.GET.get('name', None)
        phone = request.GET.get('phone', None)
        token = '1312920982:AAFxiojpbKZzznSvc3dLJwwoUgGm-QPCD28'
        chatid = '-451747147'
        message = f'Заявка с сайта Бай Эл \nИмя пользователя: {name}, \nТелефон: {phone} \n\n'

        requests.get('https://api.telegram.org/bot{}/sendMessage'.format(token), params=dict(
            chat_id=chatid,
            text=message
        ))

        return redirect('home')
