from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path, include
from django.conf.urls.i18n import i18n_patterns

from balance.views import SearchHistoryView
from bayel_project import settings
from pages.views import ReportSearchView, SendFormTelegram
from users.views import SearchResultsView, OperatorDetails

handler404 = 'pages.views.my_404'
handler403 = 'pages.views.my_403'
handler400 = 'pages.views.my_400'
handler503 = 'pages.views.my_503'

urlpatterns = [
    path('bashkaru/nurlan', admin.site.urls),
    path('api/', include('api.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('api/rest-auth/', include('rest_auth.urls')),
]

urlpatterns += i18n_patterns(
    path('', include('pages.urls')),
    path('users/', include('users.urls')),
    path('news/', include('news.urls')),
    path('balance/', include('balance.urls')),
    path('users/', include('django.contrib.auth.urls')),
    path('i18n/', include('django.conf.urls.i18n')),
    path('search/', SearchResultsView.as_view(), name='search_results'),
    path('search_history/', SearchHistoryView.as_view(), name='search_history'),
    path('report_search/', ReportSearchView.as_view(), name='report_search'),
    path('send-form-tg/', SendFormTelegram.as_view(), name='send_message'),

    prefix_default_language=False,
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS)
    # urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
