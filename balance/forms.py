from django import forms

from balance.models import History, History13


class NewHistory(forms.ModelForm):
    class Meta:
        model = History
        fields = ('history_amount', 'history_remark')


class NewHistory13(forms.ModelForm):
    class Meta:
        model = History13
        fields = ('history_amount13', 'history_remark13')

