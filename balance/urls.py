from django.urls import path

from balance.models import History
from balance.views import *

urlpatterns = [
    path('create/<int:amount_pk>/<int:operator_pk>/', HistoryCreateView.as_view(), name='history_create'),
    path('delete/<int:pk>/', HistoryDeleteView.as_view(), name='history_delete'),
    path('list/<int:user_info>/', HistoryListView.as_view(), name='histories_list'),

    path('create13/<int:amount_pk>/<int:operator_pk>/', History13CreateView.as_view(), name='history13_create'),
    path('delete13/<int:pk>/', History13DeleteView.as_view(), name='history13_delete'),
    path('list13/<int:user_info>/', History13ListView.as_view(), name='histories13_list'),

    # path to download user money history from his money history list
    path('download_list76/<int:user_his>/', export_balance76, name='export_balance76'),
    # path to download user money history from his money history list
    path('download_list13/<int:user_his>/', export_balance13, name='export_balance13'),
    # url money transactions for today
    path('his_today/', HistoryRegTodayList.as_view(), name='his_today'),
    # path to download xml file with list of money transactions for today
    path('today_money/', today_given_money, name='today_money'),
    # path to download xml file with list of money transactions for today by office
    path('today_money_office/<str:pk>/', today_given_money_office, name='today_money_office'),
    path('today_money_office_user/<str:pk>/<str:search>/', today_given_money_office_user, name='today_money_office_user'),

    # path to download xml file with list of money transactions for looking user
    path('user_bal_export/<str:pk>/', user_bal_export, name='user_bal_export'),
    # path to download xml file with list of money transactions for looking day
    path('money_his_download/', search_given_money, name='search_given_money'),
    # path to download xml file with list of money transactions for looking day or range of the days
    path('search_given_money13/', search_given_money13, name='search_given_money13'),
]
