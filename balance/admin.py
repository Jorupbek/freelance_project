from django.contrib import admin

from balance.models import History, History13


class CustomHistoryAdmin(admin.ModelAdmin):
    list_display = (
        'history_username', 'history_user_fullname', 'history_amount', 'history_remark', 'history_date',
        'history_operator', 'history_office')
    search_fields = ('history_user__icontains',)


class History13Admin(admin.ModelAdmin):
    list_display = (
        'history_username13', 'history_user_fullname13', 'history_amount13', 'history_remark13', 'history_date',
        'history_operator13', 'history_office13')
    search_fields = ('history_user13__icontains',)


admin.site.register(History, CustomHistoryAdmin)
admin.site.register(History13, History13Admin)
