from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, ListView, DeleteView
from xlwt import Borders, Alignment, Style, Pattern

from balance.forms import NewHistory, NewHistory13
from balance.models import History, History13
from users.models import CustomUser, Office

from _datetime import datetime

from datetime import date
import xlwt
from .digits_char import num2text

from django.db.models import Q
from itertools import chain
from django.db.models import Sum


class HistoryDeleteView(UserPassesTestMixin, DeleteView):
    model = History
    template_name = 'history/history_delete.html'

    def test_func(self):
        return self.request.user.is_superuser

    def get_success_url(self):
        return self.request.GET.get('next', reverse('users_list'))


class History13DeleteView(UserPassesTestMixin, DeleteView):
    model = History13
    template_name = 'history/history13_delete.html'

    def test_func(self):
        return self.request.user.is_superuser

    def get_success_url(self):
        return self.request.GET.get('next', reverse('users_list'))


class HistoryCreateView(UserPassesTestMixin, CreateView):
    form_class = NewHistory
    model = History
    template_name = 'history/history_create.html'

    def form_valid(self, form):
        his_user_id = CustomUser.objects.get(pk=self.kwargs['amount_pk'])
        oper = CustomUser.objects.get(pk=self.kwargs['operator_pk'])
        form.instance.history_operator = oper.username
        form.instance.history_office = his_user_id.user_office
        form.instance.history_username = his_user_id.username
        form.instance.history_user_fullname = his_user_id.user_fullname
        form.instance.history_user_id = his_user_id.id
        return super(HistoryCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(HistoryCreateView, self).get_context_data(**kwargs)
        top = CustomUser.objects.get(pk=self.kwargs['amount_pk'])
        context['user_info'] = top

        u_t_b = top.user_total_balance
        u_b = top.user_balance
        u_s_s = top.user_second_status
        u_id = top.id
        u_f = 0

        if u_t_b >= 15000 and not u_s_s:
            if u_t_b - u_b >= 15000:
                if u_b >= 13000:
                    user_info = CustomUser.objects.get(pk=u_id)
                    user_info.user_second_tour = True
                    user_info.save()
                u_f = u_b
                u_b = 0
            else:
                check = 15000 - (u_t_b - u_b)
                u_f = u_b - check
                if u_f >= 13000:
                    user_info = CustomUser.objects.get(pk=u_id)
                    user_info.user_second_tour = True
                    user_info.save()
                u_b = check

        context['new_balance'] = {'user_balance': u_b, 'user_frozen': u_f, 'user_total': u_t_b}
        return context

    def get_success_url(self):
        return self.request.GET.get('next', reverse('users_list'))

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_cashier


class History13CreateView(UserPassesTestMixin, CreateView):
    form_class = NewHistory13
    model = History13
    template_name = 'history/history13_create.html'

    def form_valid(self, form):
        his_user_id = CustomUser.objects.get(pk=self.kwargs['amount_pk'])
        oper = CustomUser.objects.get(pk=self.kwargs['operator_pk'])
        form.instance.history_operator13 = oper.username
        form.instance.history_office13 = his_user_id.user_office
        form.instance.history_username13 = his_user_id.username
        form.instance.history_user_fullname13 = his_user_id.user_fullname
        form.instance.history_user_id13 = his_user_id.id
        return super(History13CreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(History13CreateView, self).get_context_data(**kwargs)
        top = CustomUser.objects.get(pk=self.kwargs['amount_pk'])
        context['user_info'] = top
        return context

    def get_success_url(self):
        return self.request.GET.get('next', reverse('users_list'))

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_cashier


class HistoryListView(UserPassesTestMixin, ListView):
    template_name = 'history/histories_list.html'
    paginate_by = 100

    def get_queryset(self, **kwargs):
        user = CustomUser.objects.get(pk=self.kwargs['user_info'])
        return History.objects.filter(history_username=user.username).order_by('-id')

    def get_context_data(self, **kwargs):
        context = super(HistoryListView, self).get_context_data(**kwargs)
        context['user_in'] = CustomUser.objects.get(pk=self.kwargs['user_info'])
        return context

    def test_func(self, **kwargs):
        use = CustomUser.objects.get(pk=self.kwargs['user_info'])
        return self.request.user.is_superuser or self.request.user.is_cashier or \
               self.request.user == use or self.request.user == use.user_by_whom


class History13ListView(UserPassesTestMixin, ListView):
    template_name = 'history/histories13_list.html'
    paginate_by = 100

    def get_queryset(self, **kwargs):
        user = CustomUser.objects.get(pk=self.kwargs['user_info'])
        return History13.objects.filter(history_username13=user.username).order_by('-id')

    def get_context_data(self, **kwargs):
        context = super(History13ListView, self).get_context_data(**kwargs)
        context['user_in'] = CustomUser.objects.get(pk=self.kwargs['user_info'])
        return context

    def test_func(self, **kwargs):
        use = CustomUser.objects.get(pk=self.kwargs['user_info'])
        return self.request.user.is_superuser or self.request.user.is_cashier or \
               self.request.user == use or self.request.user == use.user_by_whom


class HistoryRegTodayList(UserPassesTestMixin, ListView):
    template_name = 'history/history_today.html'
    login_url = 'registration/login.html'
    paginate_by = 30

    def get_queryset(self):
        for7500 = History.objects.filter(history_date__icontains=date.today())
        for1300 = History13.objects.filter(history_date__icontains=date.today())
        new_histories = sorted(chain(for7500, for1300), key=lambda his: his.history_date, reverse=True)
        return new_histories

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_cashier


class SearchHistoryView(UserPassesTestMixin, ListView):
    template_name = 'history/history_today.html'

    def test_func(self):
        return self.request.user.is_staff or self.request.user.is_cashier

    def get_queryset(self):
        query = self.request.GET.get('query')
        office = self.request.GET.get('user_office')
        object_list = History.objects.filter(history_date__contains=date.today()).order_by('-id')
        for1300 = History13.objects.filter(history_date__icontains=date.today())
        object_list = sorted(chain(object_list, for1300), key=lambda his: his.history_date, reverse=True)
        if office and query:
            object_list = History.objects.filter(Q(history_date__contains=date.today()) &
                                                 Q(history_username__contains=query) &
                                                 Q(history_office__contains=office)).order_by('-id')
            object_list13 = History13.objects.filter(Q(history_date__contains=date.today()) &
                                                 Q(history_username13__contains=query) &
                                                 Q(history_office13__contains=office)).order_by('-id')
            object_list = sorted(chain(object_list, object_list13), key=lambda his: his.history_date, reverse=True)
        elif not office and query:
            object_list = History.objects.filter(
                Q(history_date__contains=date.today()) & Q(history_username__contains=query)).order_by('-id')
            object_list13 = History13.objects.filter(
                Q(history_date__contains=date.today()) & Q(history_username13__contains=query)).order_by('-id')
            object_list = sorted(chain(object_list, object_list13), key=lambda his: his.history_date, reverse=True)
        elif office and not query:
            object_list = History.objects.filter(
                Q(history_date__contains=date.today()) & Q(history_office__contains=office)).order_by(
                '-id')
            object_list13 = History13.objects.filter(
                Q(history_date__contains=date.today()) & Q(history_office13__contains=office)).order_by(
                '-id')
            object_list = sorted(chain(object_list, object_list13), key=lambda his: his.history_date, reverse=True)
        return object_list

    def get_context_data(self, **kwargs):
        context = super(SearchHistoryView, self).get_context_data(**kwargs)
        try:
            offi = Office.objects.filter(office_name__contains=self.request.GET.get('user_office'))
            if len(offi) == 1:
                context['user_office_context'] = offi[0]
        except ValueError as e:
            print('value error', e)
        context['search_input'] = self.request.GET.get('query')
        if self.request.GET.get('query') != '':
            user_info = History.objects.filter(Q(history_date__contains=date.today()) & Q(
                history_username__contains=self.request.GET.get('query')))
            if len(user_info) > 0:
                context['user_info'] = user_info[0].history_username
        return context


def export_balance_xls(request, us_id, type_model):
    user_his_id = CustomUser.objects.get(pk=us_id)
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Bayel-{name}-{date}.xls"'.format(
        name=user_his_id.username,
        date=datetime.now().strftime(
            '%d-%m-%Y'))

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('История денежных средств')

    # Sheet header, first row

    def gray_table(ws_table, row_table):
        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        font_style.font.height = 220
        font_style.alignment.horz = Alignment.HORZ_CENTER
        font_style.alignment.vert = Alignment.VERT_CENTER
        pattern = xlwt.Pattern()
        pattern.pattern = Pattern.SOLID_PATTERN
        pattern.pattern_fore_colour = Style.colour_map['gray50']
        font_style.pattern = pattern
        font_style.font.colour_index = 1
        font_style.borders.top = Borders.THIN
        font_style.borders.right = Borders.THIN
        font_style.borders.bottom = Borders.THIN
        font_style.borders.left = Borders.THIN
        ws_table.row(row_table).height = 400
        return font_style

    row_num = 0
    ws.col(0).width = 25 * 200
    ws.col(1).width = 25 * 330
    ws.col(2).width = 25 * 330

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.font.height = 150
    font_style.alignment.horz = Alignment.HORZ_RIGHT
    font_style.alignment.vert = Alignment.VERT_CENTER
    column = ['Общество с ограниченной ответственностью "Хаои"']
    ws.row(row_num).height = 250
    ws.merge(row_num, row_num, 1, 2)
    ws.write(row_num, 1, column, font_style)
    row_num += 2

    column = ['Социальная программа взаимопомощи "Бай Эл.kg"']
    ws.row(row_num).height = 250
    ws.merge(row_num, row_num, 1, 2)
    ws.write(row_num, 1, column, font_style)
    row_num += 2

    font_style = xlwt.XFStyle()
    font_style.font.height = 150
    font_style.alignment.horz = Alignment.HORZ_RIGHT
    font_style.alignment.vert = Alignment.VERT_CENTER
    column = ['Дата: {date}'.format(date=datetime.now().strftime('%d-%m-%Y'))]
    ws.write(row_num, 2, column, font_style)
    row_num += 2

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.font.height = 220
    font_style.alignment.horz = Alignment.HORZ_CENTER
    font_style.alignment.vert = Alignment.VERT_CENTER
    column = ['История выплат']
    ws.row(row_num).height = 350
    ws.merge(row_num, row_num, 0, 2)
    ws.write(row_num, 0, column, font_style)
    row_num += 2

    font = gray_table(ws, row_num)
    columns = ['Логин', 'ФИО', 'Всего полученно(Баллов)', ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font)
    row_num += 1

    font_style = xlwt.XFStyle()
    font_style.alignment.horz = Alignment.HORZ_CENTER
    font_style.alignment.vert = Alignment.VERT_CENTER
    font_style.borders.top = Borders.THIN
    font_style.borders.right = Borders.THIN
    font_style.borders.bottom = Borders.THIN
    font_style.borders.left = Borders.THIN
    name = user_his_id.username
    full_name = user_his_id.user_fullname
    if type_model == 76:
        balance = user_his_id.user_total_balance
    else:
        balance = user_his_id.user_total_balance13
    user = [name, full_name, balance]
    for col_num in range(len(user)):
        ws.write(row_num, col_num, user[col_num], font_style)
    row_num += 3

    font = gray_table(ws, row_num)
    columns = ['Берилген күнү', 'Сумма(Баллы)', 'Билдирүү']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    font_style.alignment.horz = Alignment.HORZ_CENTER
    font_style.alignment.vert = Alignment.VERT_CENTER
    font_style.borders.top = Borders.THIN
    font_style.borders.right = Borders.THIN
    font_style.borders.bottom = Borders.THIN
    font_style.borders.left = Borders.THIN
    if type_model == 76:
        rows = History.objects.filter(history_username=user_his_id.username) \
            .values_list('history_date', 'history_amount', 'history_remark')
    else:
        rows = History13.objects.filter(history_username13=user_his_id.username) \
            .values_list('history_date', 'history_amount13', 'history_remark13')

    rows = [[x.strftime("%d-%m-%Y  %H:%M") if isinstance(x, datetime) else x for x in row] for row in rows]
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    row_num += 2

    ws.set_footer_str('bayel.kg'.encode('utf-8'))
    wb.save(response)
    return response


def export_balance76(request, **kwargs):
    us_id = kwargs['user_his']
    return export_balance_xls(request, us_id, 76)


def export_balance13(request, **kwargs):
    us_id = kwargs['user_his']
    return export_balance_xls(request, us_id, 13)


def export_today_history(request, history_model, total, **kwargs):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Vedomost\'_{name}_{date}.xls"' \
        .format(name='Bayel',
                date=datetime.now().strftime('%d-%m-%Y'))
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('История выплат')

    ws.col(0).width = 25 * 75
    ws.col(1).width = 25 * 150
    ws.col(2).width = 25 * 300
    ws.col(3).width = 25 * 75
    ws.col(4).width = 25 * 225
    ws.col(5).width = 25 * 200
    ws.col(6).width = 25 * 175

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.font.height = 150
    font_style.alignment.horz = Alignment.HORZ_CENTER
    font_style.alignment.vert = Alignment.VERT_CENTER
    column = ['Общество с ограниченной ответственностью "Хаои"']
    ws.row(row_num).height = 250
    ws.merge(row_num, row_num, 4, 6)
    ws.write(row_num, 4, column, font_style)
    row_num += 2

    column = ['Социальная программа взаимопомощи "Бай Эл.kg"']
    ws.row(row_num).height = 250
    ws.merge(row_num, row_num, 4, 6)
    ws.write(row_num, 4, column, font_style)
    row_num += 2

    font_style = xlwt.XFStyle()
    font_style.font.height = 150
    font_style.alignment.horz = Alignment.HORZ_CENTER
    font_style.alignment.vert = Alignment.VERT_CENTER
    font_style.borders.top = Borders.THIN
    font_style.borders.right = Borders.THIN
    font_style.borders.bottom = Borders.THIN
    font_style.borders.left = Borders.THIN
    columns1 = [['Номер документа', 'Дата составления', ], ['', '{date}'.format(date=datetime.now().strftime(
        '%d-%m-%Y'))]]
    for row in columns1:
        for col_num in range(len(row)):
            ws.write(row_num, col_num + 5, row[col_num], font_style)
        row_num += 1
    row_num += 1

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.font.height = 220
    font_style.alignment.horz = Alignment.HORZ_CENTER
    font_style.alignment.vert = Alignment.VERT_CENTER
    column = ['Платежная ведомость']
    ws.row(row_num).height = 350
    ws.merge(row_num, row_num, 0, 6)
    ws.write(row_num, 0, column, font_style)
    row_num += 2

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.font.height = 160
    ws.row(row_num).height_mismatch = True
    ws.row(row_num).height = 450
    font_style.borders.top = Borders.THIN
    font_style.borders.right = Borders.THIN
    font_style.borders.left = Borders.THIN
    font_style.borders.bottom = Borders.THIN

    pattern = xlwt.Pattern()
    pattern.pattern = Pattern.SOLID_PATTERN
    pattern.pattern_fore_colour = Style.colour_map['gray50']
    font_style.pattern = pattern
    font_style.alignment.horz = Alignment.HORZ_CENTER
    font_style.alignment.vert = Alignment.VERT_CENTER
    font_style.font.colour_index = 1

    columns1 = ['№п/п', 'Табельный №', 'ФИО', 'Сумма', 'Прописью', 'Офис', 'Роспись']
    for col_num in range(len(columns1)):
        ws.write(row_num, col_num, columns1[col_num], font_style)
    row_num += 1

    try:
        if kwargs['history_model13']:
            """Header for 7500 list histories."""
            font_style = xlwt.XFStyle()
            font_style.font.bold = True
            font_style.font.height = 190
            ws.row(row_num).height_mismatch = True
            ws.row(row_num).height = 450
            font_style.borders.top = Borders.THIN
            font_style.borders.right = Borders.THIN
            font_style.borders.left = Borders.THIN
            font_style.borders.bottom = Borders.THIN
            pattern = xlwt.Pattern()
            pattern.pattern = Pattern.SOLID_PATTERN
            pattern.pattern_fore_colour = Style.colour_map['gray50']
            font_style.pattern = pattern
            font_style.alignment.horz = Alignment.HORZ_CENTER
            font_style.alignment.vert = Alignment.VERT_CENTER
            font_style.font.colour_index = 1
            column = ['Список 7500']
            ws.merge(row_num, row_num, 0, 5)
            ws.write(row_num, 0, column, font_style)
            row_num += 1
    except:
        pass

    """Filling the table with queryset data for 7500 histories"""
    font_style = xlwt.XFStyle()
    font_style.borders.top = Borders.THIN
    font_style.borders.right = Borders.THIN
    font_style.borders.bottom = Borders.THIN
    font_style.borders.left = Borders.THIN
    ws.row(row_num).height = 300

    font_style.font.height = 160
    font_style.alignment.horz = Alignment.HORZ_CENTER
    font_style.alignment.vert = Alignment.VERT_CENTER
    counter = 1

    for row in history_model:
        for col_num in range(1, len(row)):
            if col_num == 1:
                ws.write(row_num, 0, counter, font_style)
                counter += 1
            ws.write(row_num, col_num, row[col_num], font_style)
        row_num += 1
        ws.row(row_num).height = 300

    try:
        if kwargs['history_model13']:
            """Header for 13000 list histories."""
            font_style = xlwt.XFStyle()
            font_style.font.bold = True
            font_style.font.height = 190
            ws.row(row_num).height_mismatch = True
            ws.row(row_num).height = 450
            font_style.borders.top = Borders.THIN
            font_style.borders.right = Borders.THIN
            font_style.borders.left = Borders.THIN
            font_style.borders.bottom = Borders.THIN
            pattern = xlwt.Pattern()
            pattern.pattern = Pattern.SOLID_PATTERN
            pattern.pattern_fore_colour = Style.colour_map['gray50']
            font_style.pattern = pattern
            font_style.alignment.horz = Alignment.HORZ_CENTER
            font_style.alignment.vert = Alignment.VERT_CENTER
            font_style.font.colour_index = 1
            column = ['Список 13000']
            ws.merge(row_num, row_num, 0, 5)
            ws.write(row_num, 0, column, font_style)
            row_num += 1

            """Filling the table with queryset data for 13000 histories"""
            font_style = xlwt.XFStyle()
            font_style.borders.top = Borders.THIN
            font_style.borders.right = Borders.THIN
            font_style.borders.bottom = Borders.THIN
            font_style.borders.left = Borders.THIN
            ws.row(row_num).height = 300

            font_style.font.height = 160
            font_style.alignment.horz = Alignment.HORZ_CENTER
            font_style.alignment.vert = Alignment.VERT_CENTER
            counter = 1

            for row in kwargs['history_model13']:
                for col_num in range(1, len(row)):
                    if col_num == 1:
                        ws.write(row_num, 0, counter, font_style)
                        counter += 1
                    ws.write(row_num, col_num, row[col_num], font_style)
                row_num += 1
                ws.row(row_num).height = 300

        if kwargs['total13']:
            total75 = total
            total = total + kwargs['total13']
    except:
        pass

    total_char = ['Итого:', ' ', ' ', '{}'.format(total), ' ', ' ', ' ']

    font_style = xlwt.XFStyle()
    font_style.borders.top = Borders.THIN
    font_style.borders.right = Borders.THIN
    font_style.borders.bottom = Borders.THIN
    font_style.borders.left = Borders.THIN
    ws.row(row_num).height = 300

    font_style.font.height = 160
    font_style.font.bold = True

    font_style.alignment.horz = Alignment.HORZ_CENTER
    font_style.alignment.vert = Alignment.VERT_CENTER
    for col_num in range(len(total_char)):
        ws.write(row_num, col_num, total_char[col_num], font_style)
    row_num += 2
    ws.row(row_num).height = 300

    """Title text under the table."""
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.font.height = 160
    font_style.alignment.vert = Alignment.VERT_CENTER
    ws.row(row_num).height = 300

    try:
        column = [
            '    За программу `7500`: {} сом.    За программу `13000`: {} сом.'.format(total75, kwargs['total13'])]
        ws.merge(row_num, row_num, 0, 3)
        ws.write(row_num, 0, column, font_style)
        row_num += 2
    except Exception as e:
        pass

    text_number = num2text(total)
    column = ['    К выдаче:  {} ({}) сом'.format(total, text_number)]
    ws.merge(row_num, row_num, 0, 3)
    ws.write(row_num, 0, column, font_style)
    row_num += 2

    column = ['             Выдал(а) по ведомости:']
    ws.merge(row_num, row_num, 0, 3)
    ws.write(row_num, 0, column, font_style)
    row_num += 2

    ws.set_footer_str('bayel.kg'.encode('utf-8'))
    wb.save(response)
    return response


def user_bal_export(request, **kwargs):
    history_model = History.objects.filter(
        Q(history_username__icontains=kwargs['pk']) & Q(history_date__contains=date.today())).order_by('-id') \
        .values_list('history_blank', 'history_username', 'history_user_fullname',
                     'history_amount', 'history_blank', 'history_blank')
    history_model13 = History13.objects.filter(
        Q(history_username13__icontains=kwargs['pk']) & Q(history_date__contains=date.today())).order_by('-id') \
        .values_list('history_blank13', 'history_username13', 'history_user_fullname13',
                     'history_amount13', 'history_blank13', 'history_blank13')

    total = history_model.aggregate(Sum('history_amount'))
    total = total['history_amount__sum']
    total13 = history_model13.aggregate(Sum('history_amount13'))
    total13 = total13['history_amount13__sum']
    kwargs = {'history_model13': history_model13, 'total13': total13}
    return export_today_history(request, history_model, total, **kwargs)


def today_given_money(request, **kwargs):
    history_model = History.objects.filter(history_date__contains=date.today()).order_by('-id') \
        .values_list('history_blank', 'history_username', 'history_user_fullname',
                     'history_amount', 'history_blank', 'history_office', 'history_blank')

    history_model13 = History13.objects.filter(history_date__contains=date.today()).order_by('-id') \
        .values_list('history_blank13', 'history_username13', 'history_user_fullname13',
                     'history_amount13', 'history_blank13', 'history_office13', 'history_blank13')

    total = history_model.aggregate(Sum('history_amount'))
    total = total['history_amount__sum']
    total13 = history_model13.aggregate(Sum('history_amount13'))
    total13 = total13['history_amount13__sum']
    kwargs = {'history_model13': history_model13, 'total13': total13}
    return export_today_history(request, history_model, total, **kwargs)


def today_given_money_office(request, **kwargs):
    history_model = History.objects.filter(
        Q(history_date__contains=date.today()) & Q(history_office=kwargs['pk'])).order_by('-id') \
        .values_list('history_blank', 'history_username', 'history_user_fullname',
                     'history_amount', 'history_blank', 'history_blank')
    history_model13 = History13.objects.filter(
        Q(history_date__contains=date.today()) & Q(history_office13=kwargs['pk'])).order_by('-id') \
        .values_list('history_blank13', 'history_username13', 'history_user_fullname13',
                     'history_amount13', 'history_blank13', 'history_blank13')

    total = history_model.aggregate(Sum('history_amount'))
    total = total['history_amount__sum']
    total13 = history_model13.aggregate(Sum('history_amount13'))
    total13 = total13['history_amount13__sum']
    kwargs = {'history_model13': history_model13, 'total13': total13}
    return export_today_history(request, history_model, total, **kwargs)


def today_given_money_office_user(request, **kwargs):
    history_model = History.objects.filter(
        Q(history_date__contains=date.today()) & Q(history_office=kwargs['pk']) & Q(
            history_username__contains=kwargs['search'])).order_by('-id') \
        .values_list('history_blank', 'history_username', 'history_user_fullname',
                     'history_amount', 'history_blank', 'history_blank')
    history_model13 = History13.objects.filter(
        Q(history_date__contains=date.today()) & Q(history_office13=kwargs['pk']) & Q(
            history_username13__contains=kwargs['search'])).order_by('-id') \
        .values_list('history_blank13', 'history_username13', 'history_user_fullname13',
                     'history_amount13', 'history_blank13', 'history_blank13')

    total = history_model.aggregate(Sum('history_amount'))
    total = total['history_amount__sum']
    total13 = history_model13.aggregate(Sum('history_amount13'))
    total13 = total13['history_amount13__sum']
    kwargs = {'history_model13': history_model13, 'total13': total13}
    return export_today_history(request, history_model, total, **kwargs)


# function for download xml file with money transactions list for looking day
def search_given_money(request, **kwargs):
    # start date from report_search template
    date_looking = request.GET.get('date')
    # end date from report_search template
    end_date = request.GET.get('end_date')
    user_office = request.GET.get('user_office')
    """if end date was not specified from report_search template we will search for histories only by start day"""
    if user_office == '':
        if end_date == '':
            history_model2 = History.objects.filter(history_date__contains=date_looking).order_by('-id') \
                .values_list('history_blank', 'history_username', 'history_user_fullname', 'history_amount',
                             'history_blank', 'history_blank', )
            total = 0
            his = list(history_model2)
            for x in his:
                total += x[3]
        else:
            """if end date was specified from report_search template we will search for histories range 
            from start date till end date inclusive"""

            history_model2 = History.objects.filter(history_date__date__range=(date_looking, end_date)).order_by(
                '-history_date').values_list('history_blank', 'history_username',
                                             'history_user_fullname',
                                             'history_amount', 'history_blank',
                                             'history_blank', )
            # count total sum of theese period
            total = 0
            his = list(history_model2)
            for x in his:
                total += x[3]
    else:
        if end_date == '':
            history_model2 = History.objects.filter(
                Q(history_date__contains=date_looking) & Q(history_office=user_office)).order_by('-id') \
                .values_list('history_blank', 'history_username',
                             'history_user_fullname',
                             'history_amount', 'history_blank',
                             'history_blank')
            total = 0
            his = list(history_model2)
            for x in his:
                total += x[3]
        else:
            """if end date was specified from report_search template we will search for histories range 
            from start date till end date inclusive"""
            history_model2 = History.objects.filter(
                Q(history_date__date__range=(date_looking, end_date)) & Q(history_office=user_office)).order_by(
                '-history_date').values_list('history_blank', 'history_username',
                                             'history_user_fullname',
                                             'history_amount', 'history_blank',
                                             'history_blank')
            # count total sum of theese period
            total = 0
            his = list(history_model2)
            for x in his:
                total += x[3]

    return export_today_history(request, history_model2, total)


# function for download xml file with money transactions list for looking day
def search_given_money13(request, **kwargs):
    # start date from report_search template
    date_looking = request.GET.get('date')
    # end date from report_search template
    end_date = request.GET.get('end_date')
    user_office = request.GET.get('user_office')
    """if end date was not specified from report_search template we will search for histories only by start day"""
    if user_office == '':
        if end_date == '':
            history_model2 = History13.objects.filter(history_date__contains=date_looking).order_by('-id') \
                .values_list('history_blank13', 'history_username13', 'history_user_fullname13', 'history_amount13',
                             'history_blank13', 'history_blank13', )
            total = 0
            his = list(history_model2)
            for x in his:
                total += x[3]
        else:
            """if end date was specified from report_search template we will search for histories range 
            from start date till end date inclusive"""

            history_model2 = History13.objects.filter(history_date__date__range=(date_looking, end_date)).order_by(
                '-history_date').values_list('history_blank13', 'history_username13',
                                             'history_user_fullname13',
                                             'history_amount13', 'history_blank13',
                                             'history_blank13', )
            # count total sum of theese period
            total = 0
            his = list(history_model2)
            for x in his:
                total += x[3]
    else:
        if end_date == '':
            history_model2 = History13.objects.filter(
                Q(history_date__contains=date_looking) & Q(history_office13=user_office)).order_by('-id') \
                .values_list('history_blank13', 'history_username13',
                             'history_user_fullname13',
                             'history_amount13', 'history_blank13',
                             'history_blank13')
            total = 0
            his = list(history_model2)
            for x in his:
                total += x[3]
        else:
            """if end date was specified from report_search template we will search for histories range 
            from start date till end date inclusive"""
            history_model2 = History13.objects.filter(
                Q(history_date__date__range=(date_looking, end_date)) & Q(history_office13=user_office)).order_by(
                '-history_date').values_list('history_blank13', 'history_username13',
                                             'history_user_fullname13',
                                             'history_amount13', 'history_blank13',
                                             'history_blank13')
            # count total sum of theese period
            total = 0
            his = list(history_model2)
            for x in his:
                total += x[3]

    return export_today_history(request, history_model2, total)

