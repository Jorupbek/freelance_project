from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.utils.datetime_safe import datetime
from users.models import CustomUser, Office


class History(models.Model):
    history_username = models.CharField(verbose_name='Логин пользователя ', max_length=100)
    history_user_fullname = models.CharField(verbose_name='ФИО пользователя', max_length=100)
    history_user_id = models.IntegerField(verbose_name='id пользователя')
    history_blank = models.CharField(default='', max_length=1, verbose_name='Пустышка')
    history_amount = models.IntegerField(verbose_name='Сумма')
    history_remark = models.TextField(verbose_name=_(u'Билдирүү'))
    history_date = models.DateTimeField(verbose_name='Дата операции', auto_now_add=True)
    history_operator = models.CharField(verbose_name='Оператор', max_length=100, null=True, blank=True)
    history_office = models.CharField(verbose_name='Офис', max_length=150, null=True, blank=True)

    def __str__(self):
        return f'{self.history_username} - {self.history_amount} -------- {self.history_date}'


class History13(models.Model):
    history_username13 = models.CharField(verbose_name='Логин пользователя 13', max_length=100)
    history_user_fullname13 = models.CharField(verbose_name='ФИО пользователя 13', max_length=100)
    history_user_id13 = models.IntegerField(verbose_name='id пользователя 13')
    history_blank13 = models.CharField(default='', max_length=1, verbose_name='Пустышка 13')
    history_amount13 = models.IntegerField(verbose_name='Сумма 13')
    history_remark13 = models.TextField(verbose_name=_(u'Билдирүү 13'))
    history_date = models.DateTimeField(verbose_name='Дата операции 13', auto_now_add=True)
    history_operator13 = models.CharField(verbose_name='Оператор 13', max_length=100, null=True, blank=True)
    history_office13 = models.CharField(verbose_name='Офис 13', max_length=150, null=True, blank=True)

    def __str__(self):
        return f'{self.history_username13} - {self.history_amount13} -------- {self.history_date}'


@receiver(post_save, sender=History)
def followers_check(sender, instance, **kwargs):
    user_bal = CustomUser.objects.get(username=instance.history_username)
    user_bal.user_balance -= int(instance.history_amount)
    user_bal.save()


@receiver(post_delete, sender=History)
def check_history(sender, instance, **kwargs):
    try:
        user_bal = CustomUser.objects.get(username=instance.history_username)
        user_bal.user_balance += int(instance.history_amount)
        user_bal.save()
    except ObjectDoesNotExist as e:
        print('Object Does not exist')


@receiver(post_save, sender=History13)
def followers_check13(sender, instance, **kwargs):
    user_bal = CustomUser.objects.get(username=instance.history_username13)
    user_bal.user_balance13 -= int(instance.history_amount13)
    user_bal.save()


@receiver(post_delete, sender=History13)
def check_history13(sender, instance, **kwargs):
    try:
        user_bal = CustomUser.objects.get(username=instance.history_username13)
        user_bal.user_balance13 += int(instance.history_amount13)
        user_bal.save()
    except ObjectDoesNotExist as e:
        print('Object Does not exist')